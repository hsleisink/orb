/* Orb notifications application
 *
 * Copyright (c) by Hugo Leisink
 *
 * Let every function in this file start with the application name
 * to avoid conflicts with other applications.
 *
 * Always use notifications_window to interact with your application to
 * avoid issues with multiple instances of your application.
 */

const NOTIFICATIONS_ICON = '/apps/notifications/notifications.png';
const NOTIFICATIONS_CHECK_INTERVAL = 5;

function notifications_menu_click(notifications_window, item) {
	switch (item) {
		case 'Export':
			orb_file_dialog('Export', function(filename) {
				var log = "";
				notifications_window.find('ul li').each(function() {
					log = $(this).text() + '\n' + log;
				});

				orb_file_exists(filename, function(exists) {
					if (exists) {
						if (confirm('File already exists. Overwrite?') == false) {
							return;
						}
					}
					orb_file_save(filename, log);
				}, function() {
					orb_file_save(filename, log);
				});
			});
			break;
		case 'Clear':
			orb_confirm('Clear all system notifications?', function() {
				$.post({
					url: '/notifications/clear'
				}).done(function(data) {
					notifications_window.find('ul').empty();
					$('div.startmenu div.system img[title="Notifications"]').attr('src', NOTIFICATIONS_ICON);
				}).fail(function() {
					orb_alert('Error clearing notifications.');
				});
			});
			break;
		case 'Exit':
			notifications_window.close();
			break;
		case 'About':
			orb_alert('<img src="' + NOTIFICATIONS_ICON + '" class="about" draggable="false" />Orb system notifications\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function notifications_open(filename = undefined) {
	var window_content =
		'<div class="notifications"><ul></ul></div>';

	var notifications_window = $(window_content).orb_window({
		header:'Orb system notifications',
		icon: NOTIFICATIONS_ICON,
		width: 600,
		height: 300,
		menu: {
			'File': [ 'Export', 'Clear', '-', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: notifications_menu_click
	});

	$.ajax({
		url: '/notifications/list'
	}).done(function(data) {
		var list = notifications_window.find('ul');

		$(data).find('notification').each(function() {
			var line = $(this).text();

			if (line.toLowerCase().indexOf('<script') == -1) {
				line = '<li>' + line + '</li>';
				list.prepend(line);
			}
		});
	});

	notifications_window.open();
}

function notifications_check(callback = undefined) {
	$.ajax({
		url: '/notifications/waiting'
	}).done(function(data) {
		var waiting = $(data).find('waiting').text();

		if (waiting == 'yes') {
			$('div.startmenu div.system img[title="Notifications"]').attr('src', '/apps/notifications/notifications_waiting.png');

			if (callback != undefined) {
				callback();
			}
		}
	});
}

$(document).ready(function() {
	orb_startmenu_system('Notifications', NOTIFICATIONS_ICON, notifications_open);

	notifications_check(function() {;
		if (parseInt($('div.desktop').attr('counter')) == 0) {
			notifications_open();
		}
	});

	window.setInterval(notifications_check, NOTIFICATIONS_CHECK_INTERVAL * 60000);
});
