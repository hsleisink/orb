<?php
	namespace Orb;

	if (defined("ORB_VERSION") == false) exit;

	class notifications extends orb_backend {
		public function get_waiting() {
			$waiting = filesize($this->home_directory."/.notifications") > 0;

			$this->view->add_tag("waiting", show_boolean($waiting));
		}

		public function get_list() {
			$notifications = array();

			$file = $this->home_directory."/.notifications";

			if (file_exists($file)) {
				if (($fp = fopen($file, "r")) != false) {
					while (($notification = fgets($fp)) !== false) {
						array_push($notifications, trim($notification));
					}

					fclose($fp);
				}
			}

			$this->view->open_tag("notifications");
			foreach ($notifications as $notification) {
				$this->view->add_tag("notification", $notification);
			}
			$this->view->close_tag();
		}

		public function post_clear() {
			$file = $this->home_directory."/.notifications";

			if (file_exists($file)) {
				if (($fp = fopen($file, "w")) == false) {
					return false;
				}

				ftruncate($fp, 0);
				fclose($fp);
			}

			return true;
		}
	}
?>
