/* Orb About application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const ABOUT_ICON = '/images/orb.png';

var about_windows_open = 0;

function about_open() {
	if (about_windows_open > 0) {
		$('div.about').parent().parent().each(function() {
			orb_window_raise($(this));
		});
		return;
	}

	var version = $('div.desktop').attr('version');
	var window_content =
		'<div class="about">' +
		'<img src="' + ABOUT_ICON +'" class="orb" draggable="false">' +
		'<p>Orb v' + version + '</p>' +
		'<p>A free and open source web desktop. ' +
		'<a href="https://gitlab.com/hsleisink/orb" target="_blank">Download here</a>.</p>' +
		'<p>Copyright &copy; by Hugo Leisink</p>' +
		'</div>';

	var about_window = $(window_content).orb_window({
		header: 'About Orb',
		width: 350,
		height: 103,
		close: function() {
			about_windows_open--;
		},
		resize: false,
		maximize: false,
		minimize: false
	});

	about_window.open();
	about_windows_open++;
}

$(document).ready(function() {
	orb_startmenu_system('About Orb', ABOUT_ICON, about_open);
});
