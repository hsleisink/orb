/* Orb 2048 application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const GAME_2048_ICON = '/apps/2048/2048.png';

function game_2048_restart(game_2048_window) {
	game_2048_window.find('iframe').contents().find('a.restart-button')[0].click();
}

function game_2048_focus(game_2048_window) {
	game_2048_window.find('iframe')[0].contentWindow.focus();
}

function game_2048_menu_click(game_2048_window, item) {
	switch (item) {
		case 'New game':
			game_2048_restart(game_2048_window);
			game_2048_focus(game_2048_window);
			break;
		case 'Exit':
			game_2048_window.close();
			break;
		case 'About':
			var message = [
				'<img src="' + GAME_2048_ICON + '" class="about" draggable="false" />2048 puzzle',
				'Copyright (c) by Gabriele Cirulli',
				'<a href="https://github.com/gabrielecirulli/2048">https://github.com/gabrielecirulli/2048</a>'
			];
			orb_alert(message.join('\n'), 'About');
			break;
	}
}

function game_2048_open(filename = undefined) {
	var window_content =
		'<div class="game_2048">' +
		'<iframe src="/apps/2048/2048.html"></iframe>' +
		'</div>';

	var game_2048_window = $(window_content).orb_window({
		header:'2048',
		icon: GAME_2048_ICON,
		width: 400,
		height: 600,
		resize: false,
		menu: {
			'Game': [ 'New game', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: game_2048_menu_click,
		close: function() {
			game_2048_restart(game_2048_window);
		}
	});

	game_2048_window.open();

	game_2048_focus(game_2048_window);
}

$(document).ready(function() {
	orb_startmenu_add('2048', GAME_2048_ICON, game_2048_open);
});
