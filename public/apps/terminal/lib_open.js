/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

function terminal_open(term, files) {
	if (files.length == 0) {
		files = [ '' ];
	}

	files.forEach(function(file) {
		file = term.path + '/' + file;

		orb_file_type(file, function(type) {
			if (type == 'file') {
				var extension = orb_file_extension(file);
				var handler = orb_get_file_handler(extension);

				if (handler != undefined) {
					handler(file);
				} else {
					window.open(orb_download_url(file));
				}

				terminal_done(term);
			} else {
				var handler = orb_get_directory_handler();
				handler(file);

				terminal_done(term);
			}
		}, function() {
			term.writeln('File not found.');
			terminal_done(term);
		});
	});
}
