/* Orb Life application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const LIFE_ICON = '/apps/life/life.png';

const LIFE_RULE = 2333;
const LIFE_CELL_SIZE = 10;
const LIFE_DEAD = 0;
const LIFE_ALIVE = 1;
const LIFE_DELAY = 3;

function life_board(life_window, board) {
	try {
		board = JSON.parse(board);
	} catch (e) {
		orb_alert('Invalid Life file.');
		return;
	}

	if (Array.isArray(board) == false) {
		orb_alert('Invalid Life file.');
		return;
	}

	life_window.data('board', board);
	life_draw(life_window);
}

function life_menu_click(life_window, item) {
	switch (item) {
		case 'New':
			orb_confirm('Are you sure you want to remove all cells?', function() {
				if (life_window.data('pause') == false) {
					life_window.find('input.pause').trigger('click');
				}

				life_window.data('board', []);
				life_draw(life_window);
			});
			break;
		case 'Open':
			orb_file_dialog('Load', function(filename) {
				if (filename.substr(filename.length - 5) != '.life') {
					orb_alert('Invalid file.');
				} else orb_file_open(filename, function(board) {
					if (life_window.data('pause') == false) {
						life_window.find('input.pause').trigger('click');
					}
					life_board(life_window, board);
				});
			}, 'Documents/Life');
			break;
		case 'Save':
			var board = life_window.data('board');

			orb_file_dialog('Save', function(filename) {
				if (filename.substr(filename.length - 5) != '.life') {
					filename += '.life';
				}

				orb_file_exists(filename, function(exists) {
					if (exists) {
						if (confirm('File already exists. Overwrite?') == false) {
							return;
						}
					}
					orb_file_save(filename, JSON.stringify(board))
				});
			}, 'Documents/Life');
			break;
		case 'Exit':
			life_window.close();
			break;
		case 'About':
			orb_alert('<img src="' + LIFE_ICON + '" class="about" draggable="false" />Life\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function life_rule(life_window, rule) {
	rule = rule.toString();

	life_window.data('die_min', parseInt(rule.substr(0, 1)));
	life_window.data('die_max', parseInt(rule.substr(1, 1)));
	life_window.data('live_min', parseInt(rule.substr(2, 1)));
	life_window.data('live_max', parseInt(rule.substr(3, 1)));
}

function life_live(life_window, x, y) {
	var board = life_window.data('board');

	if (board[y] == undefined) {
		board[y] = [];
	}
	board[y][x] = LIFE_ALIVE;

	life_window.data('board', board);
}

function life_die(life_window, x, y) {
	var board = life_window.data('board');

	if (board[y] == undefined) {
		board[y] = [];
	}
	board[y][x] = LIFE_DEAD;

	life_window.data('board', board);
}

function life_board_size(life_window) {
	var width = life_window.innerWidth();
	var height = life_window.innerHeight() - 40;

	var hor_new = Math.round(width / (LIFE_CELL_SIZE + 1));
	var ver_new = Math.round(height / (LIFE_CELL_SIZE + 1));

	life_window.find('table tfoot td').attr('colspan', hor_new);

	var ver_cur = life_window.find('table tr').length;

	var table = life_window.find('table tbody');

	while (ver_cur > ver_new) {
		table.find('tr:last-child').remove();
		ver_cur--;
	}

	while (ver_cur < ver_new) {
		table.append('<tr></tr>');
		ver_cur++;
	}

	table.find('tr').each(function() {
		var hor_cur = $(this).find('td').length;

		while (hor_cur > hor_new) {
			$(this).find('td:last-child').remove();
			hor_cur--;
		}

		while (hor_cur < hor_new) {
			$(this).append('<td></td>');
			hor_cur++;
		}
	});

	var board = life_window.data('board');
	board = board.slice(0, hor_new);
	for (y = 0; y < hor_new; y++) {
		if (board[y] != undefined) {
    		board[y] = board[y].slice(0, ver_new);
		}
	}
	life_window.data('board', board);

	table.find('td').off('mousedown');
	table.find('td').on('mousedown', function(event) {
		if (event.button != 0) {
			return;
		}

		var toggle = function(cell, alive) {
			var x = cell.index();
			var y = cell.parent().index();

			if (alive) {
				life_die(life_window, x, y);
				cell.removeClass('alive');
			} else {
				life_live(life_window, x, y);
				cell.addClass('alive');
			}
		}

		var board = life_window.data('board');
		var x = $(this).index();
		var y = $(this).parent().index();

		if (board[y] == undefined) {
			var alive = false;
		} else {
			var alive = (board[y][x] === LIFE_ALIVE);
		}

		toggle($(this), alive);

		if (life_window.data('pause') === true) {
			table.find('td').on('mouseenter', function() {
				toggle($(this), alive);
			});

			table.on('mouseup', function() {
				table.find('td').off('mouseenter');
			});
		}
	});
}

function life_move(life_window) {
	if ((life_window.data('pause') === true) || (life_window.is(':visible') == false)) {
		return;
	}

	var die_min = life_window.data('die_min');
	var die_max = life_window.data('die_max');
	var live_min = life_window.data('live_min');
	var live_max = life_window.data('live_max');

	var max_y = life_window.find('table tbody tr').length;
	var max_x = (max_y == 0) ? 0 : life_window.find('table tbody tr:first-child td').length;

	var board = life_window.data('board');
	var wrap = life_window.data('wrap');

	var board_new = [];
	for (y = 0; y < max_y; y++) {
		if (board[y] != undefined) {
    		board_new[y] = board[y].slice();
		}
	}

	for (y = 0; y < max_y; y++) {
		if (board_new[y] == undefined) {
			board[y] = [];
			board_new[y] = [];
		}

		for (x = 0; x < max_x; x++) {
			var alive = (board[y][x] === LIFE_ALIVE);

			var neighbours = alive ? -1 : 0;
			for (dy = -1; dy <= 1; dy++) {
				var cy = y + dy;
				if (wrap) {
					if (cy < 0) {
						cy = max_y - 1;
					} else if (cy >= max_y) {
						cy = 0;
					}
				}

				if (board[cy] == undefined) {
					continue;
				}

				for (dx = -1; dx <= 1; dx++) {
					var cx = x + dx;
					if (wrap) {
						if (cx < 0) {
							cx = max_x - 1;
						} else if (cx >= max_x) {
							cx = 0;
						}
					}

					if (board[cy][cx] === LIFE_ALIVE) {
						neighbours++;
					}
				}
			}

			if (alive) {
				if ((neighbours < die_min) || (neighbours > die_max)) {
					board_new[y][x] = LIFE_DEAD;
				}
			} else {
				if ((neighbours >= live_min) && (neighbours <= live_max)) {
					board_new[y][x] = LIFE_ALIVE;
				}
			}
		}
	}

	life_window.data('board', board_new);

	life_draw(life_window);

	window.setTimeout(function() {
		life_move(life_window);
	}, life_window.data('delay'));
}

function life_draw(life_window) {
	var board = life_window.data('board');

	var y = 0;

	life_window.find('table tr').each(function() {
		var row = $(this);

		if (board[y] == undefined) {
			row.find('td').removeClass('alive');
		} else {
			var x = 0;

			row.find('td').each(function() {
				if (board[y][x] === LIFE_ALIVE) {
					$(this).addClass('alive');
				} else {
					$(this).removeClass('alive');
				}

				x++;
			});
		}

		y++;
	});
}

function life_calculate_delay(value) {
	return Math.round(Math.exp(6.2 - (value / 2))) + 5;
}

function life_open(filename = undefined) {
	var controls = '<div class="btn-group">' +
		'<input type="button" value="Pause" class="btn btn-primary btn-sm pause" />' + 
		'<input type="button" value="Wrap" class="btn btn-primary btn-sm wrap" />' + 
		'</div><div class="speed"><div class="slider">' +
  		'<div class="ui-slider-handle"></div>' +
		'</div></div></div>';

	var window_content =
		'<div class="life">' +
		'<table class="board">' +
		'<tbody></tbody>' + 
		'<tfoot><tr><td>' + controls + '</td></tr></tfoot>' +
		'</table>' +
		'</div>';

	var life_window = $(window_content).orb_window({
		header:'Life',
		icon: LIFE_ICON,
		width: 600,
		height: 400,
		menu: {
			'Game': [ 'New', 'Open', 'Save', '-', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: life_menu_click,
		resize: function() {
			life_board_size(life_window);
		},
		stop: function() {
		}
	});

    life_window.find("div.slider").slider({
		min: 0,
		max: 9,
		value: LIFE_DELAY,
		create: function() {
			life_window.find('div.ui-slider-handle').text($(this).slider('value') + 1);
		},
		slide: function(event, ui) {
			life_window.find('div.ui-slider-handle').text(ui.value + 1);
			life_window.data('delay', life_calculate_delay(ui.value));
		}
	});

	life_window.find('tfoot input.pause').on('click', function() {
		if (life_window.data('pause') === true) {
			life_window.data('pause', false);
			life_move(life_window);
			$(this).removeClass('btn-primary');
			$(this).addClass('btn-default');
		} else {
			life_window.data('pause', true);
			$(this).removeClass('btn-default');
			$(this).addClass('btn-primary');
		}
	});

	life_window.find('tfoot input.wrap').on('click', function() {
		var wrap = (life_window.data('wrap') === false);
		life_window.data('wrap', wrap);

		if (wrap) {
			$(this).removeClass('btn-default');
			$(this).addClass('btn-primary');
		} else {
			$(this).removeClass('btn-primary');
			$(this).addClass('btn-default');
		}
	});

	life_window.data('board', []);
	life_window.data('pause', true);
	life_window.data('delay', life_calculate_delay(LIFE_DELAY));
	life_window.data('wrap', true);

	life_window.open();

	life_rule(life_window, LIFE_RULE);
	life_board_size(life_window);

	if (filename != undefined) {
		orb_file_open(filename, function(board) {
			life_board(life_window, board);
		});
	}

	window.setTimeout(function() {
		life_move(life_window);
	}, life_window.data('delay'));
}

$(document).ready(function() {
	orb_startmenu_add('Life', LIFE_ICON, life_open);
	orb_upon_file_open('life', life_open, LIFE_ICON);
});
