/* Orb DosBox application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const DOSBOX_ICON = '/apps/dosbox/dosbox.png';

function dosbox_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	return extension.toLowerCase() == 'jsdos';
}

function dosbox_start_bundle(dosbox_window, bundle = undefined) {
	dosbox_window.empty();

	var iframe = '<iframe src="/apps/dosbox/dosbox.php';
	if (bundle != undefined) {
		iframe += '?bundle=' + bundle;
	}
	iframe += '"></iframe>';

	dosbox_window.append(iframe);
}

function dosbox_open(filename = undefined) {
	var window_content =
		'<div class="dosbox"><p>Select File &Rightarrow; Open to open a .jsdos file.</p></div>';

	var dosbox_window = $(window_content).orb_window({
		header:'DosBox',
		icon: DOSBOX_ICON,
		width: 731,
		height: 400,
		menu: {
			'File': [ 'Open', 'Exit' ],
			'Download': [ 'Abandonware DOS', 'Classic Reload', 'DOS Games', 'DOS Games Archive', '-', 'Create DosBox file' ],
			'Help': [ 'About' ]
		},
		menuCallback: dosbox_menu_click,
	});

	dosbox_start_bundle(dosbox_window, filename);
	dosbox_window.open();
}

function dosbox_menu_click(dosbox_window, item) {
	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				if (dosbox_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					dosbox_start_bundle(dosbox_window, filename);
				}
			}, 'Emulators/DosBox');
			break;
		case 'Exit':
			dosbox_window.close();
			break;
		case 'Abandonware DOS':
			window.open('https://www.abandonwaredos.com/', '_blank');
			break;
		case 'Classic Reload':
			window.open('https://classicreload.com/', '_blank');
			break;
		case 'DOS Games':
			window.open('https://dosgames.com/', '_blank');
			break;
		case 'DOS Games Archive':
			window.open('https://www.dosgamesarchive.com/', '_blank');
			break;
		case 'Create DosBox file':
			window.open('https://dos.zone/studio/', '_blank');
			break;
		case 'About':
			var message = [
				'<img src="' + DOSBOX_ICON + '" class="about" draggable="false" />DOS emulator',
				'Copyright (c) by Hugo Leisink\n',
				'js-dos',
				'Copyright (c) by caiiiycuk',
				'<a href="https://js-dos.com/" target="_blank">https://js-dos.com/</a>\n',
				'DOSBox',
				'Copyright (c) by the DOSBox crew',
				'<a href="https://www.dosbox.com/" target="_blank">https://www.dosbox.com/</a>'
			];
			orb_alert(message.join('\n'), 'About');
			break;
	}
}

$(document).ready(function() {
	orb_startmenu_add('DosBox', DOSBOX_ICON, dosbox_open);
	orb_upon_file_open('jsdos', dosbox_open, DOSBOX_ICON);
});
