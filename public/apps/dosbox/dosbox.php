<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/apps/dosbox/js-dos.css" />
<script type="text/javascript" src="/apps/dosbox/js-dos.js"></script>
<script type="text/javascript">
	function run_dosbox() {
		emulators.pathPrefix = "/apps/dosbox/";
<?php
		if (isset($_GET["bundle"])) {
			$bundle = "/orb/file/download/".urlencode($_GET["bundle"]);
		} else {
			$bundle = "/apps/dosbox/empty.jsdos";
		}
		printf("\t\tDos(document.getElementById('dosbox')).run('%s');\n", $bundle);
?>
	}
</script>
<style type="text/css">
	html, body, div#dosbox {
		width:100%;
		height:100%;
		margin:0;
		padding:0;
	}
</style>
</head>
<body onLoad="javascript:run_dosbox()">
<div id="dosbox"></div>
</body>
</html>
