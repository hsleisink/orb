/* Orb PDF application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const PDF_ICON = '/apps/pdf/pdf.png';

function pdf_open_file(pdf_window, filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return;
	}

	if (extension.toLowerCase() != 'pdf') {
		orb_alert('Not a PDF file.');
		return;
	}

	pdf_window.find('iframe').attr('src', orb_download_url(filename));

	pdf_window.data('filename', filename);

	pdf_window.set_header(filename);
}

function pdf_menu_click(pdf_window, item) {
	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				pdf_open_file(pdf_window, filename);
			}, 'Documents');
			break;
		case 'Download':
			var filename = pdf_window.data('filename');
			if (filename == undefined) {
				return;
			}

			var url = orb_download_url(filename);
			window.open(url, '_blank').focus();
			break;
		case 'Exit':
			pdf_window.close();
			break;
		case 'About':
			orb_alert('<img src="' + PDF_ICON + '" class="about" draggable="false" />PDF viewer\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function pdf_open(filename = undefined) {
	var window_content =
		'<div class="pdf"><iframe></iframe></div>';

	var pdf_window = $(window_content).orb_window({
		header:'PDF',
		icon: PDF_ICON,
		width: 500,
		height: 600,
		menu: {
			'File': [ 'Open', 'Download', '-', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: pdf_menu_click
	});

	pdf_window.open();

	if (filename != undefined) {
		pdf_open_file(pdf_window, filename);
	}
}

$(document).ready(function() {
	orb_startmenu_add('PDF', PDF_ICON, pdf_open);
	orb_upon_file_open('pdf', pdf_open, PDF_ICON);
});
