/* Orb Video application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const VIDEO_ICON = '/apps/video/video.png';

var video_extensions = [ 'avi', 'mpg', 'mpeg', 'mp4', 'mov', 'webm', 'wmv' ];

function video_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	return video_extensions.includes(extension.toLowerCase());
}

function video_play(video_window, filename) {
	video_window.find('video').remove();
	video_window.append('<video controls><source src="' + orb_download_url(filename) + '" /></video>');
	video_window.find('video')[0].play();

	video_window.set_header(filename);
}

function video_menu_click(video_window, item) {
	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				if (video_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					video_play(video_window, filename);
				}
			});
			break;
		case 'Exit':
			video_window.close();
			break;
		case 'About':
			orb_alert('<img src="' + VIDEO_ICON + '" class="about" draggable="false" />Video player\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function video_open(filename = undefined) {
	var window_content =
		'<div class="video">' +
		'<div class="name"></div>' +
		'<video controls></video>' +
		'</div>';

	var video_window = $(window_content).orb_window({
		header:'Video',
		icon: VIDEO_ICON,
		width: 500,
		height: 270,
		menu: {
			'File': [ 'Open', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: video_menu_click
	});

	video_window.open();

	if (filename != undefined) {
		video_play(video_window, filename);
	}
}

$(document).ready(function() {
	orb_startmenu_add('Video', VIDEO_ICON, video_open);
	video_extensions.forEach(function(extension) {
		orb_upon_file_open(extension, video_open, VIDEO_ICON);
	});
});
