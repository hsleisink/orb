/* Orb Paint application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const PAINT_ICON = '/apps/paint/paint.png';

const PAINT_DRAW   = 1;
const PAINT_ERASE  = 2;
const PAINT_CROP   = 3;
const PAINT_LINE   = 4;
const PAINT_SQUARE = 5;
const PAINT_CIRCLE = 6;
const PAINT_PICKER = 7;

var autosave_base = 'Temporary/paint_autosave_';
var paint_valid_extensions = [ 'gif', 'jpeg', 'jpg', 'png', 'webp' ];

function paint_dont_discard(paint_window) {
	if (paint_window.data('changed')) {
		if (confirm('Picture has been changed. Discard?') == false) {
			return true;
		}
	}

	return false;
}

function paint_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	return paint_valid_extensions.includes(extension.toLowerCase());
}

function paint_rgb_to_hex(color) {
	if (color.substr(0, 4) == 'rgb(') {
		var start = 4;
	} else if (color.substr(0, 5) == 'rgba(') {
		var start = 5;
	} else {
		return color;
	}

	color = color.substr(start);
	color = color.substr(0, color.length - 1);
	color = color.replace(/ /g, '');
	var rgb = color.split(',');

	var result = '#';

	for (var i = 0; i < 3; i++) {
		result += parseInt(rgb[i]).toString(16).padStart(2, '0');
	}

	return result;
}

function paint_select_color(span) {
	var default_color = span.css('background-color');
	default_color = paint_rgb_to_hex(default_color);

	var color_content =
		'<div class="paint_color">' +
		'<input type="text" class="color" name="color" value="' + default_color + '" />' +
		'<div class="colorpicker"></div>' +
		'<div class="btn-group">' +
		'<button class="btn btn-default save">Save</button>' +
		'<button class="btn btn-default default">Default</button>' +
		'</div>' +
		'</div>';

	var color_window = $(color_content).orb_window({
		header:'Select color',
		width: 220,
		height: 270,
		maximize: false,
		minimize: false,
		resize: false,
		dialog: true
	});

	orb_load_javascript('/apps/paint/picker/farbtastic.js');
	orb_load_stylesheet('/apps/paint/picker/farbtastic.css');

	var farbtastic = color_window.find('div.colorpicker').farbtastic('input.color');

	color_window.find('button.save').click(function() {
		var color = color_window.find('input.color').val();
		span.css('background-color', color);
		color_window.close();
		span.trigger('click');
	});

	color_window.find('button.default').click(function() {
		jQuery.farbtastic('div.colorpicker').setColor(default_color);
	});

	color_window.open();
}

function paint_init_options(paint_window) {
	/* Draw width
	 */
	var draw_handle = paint_window.find('div.drawwidth div div');

	paint_window.find('div.drawwidth > div').slider({
		min: 1,
		max: 100,
		create: function() {
			draw_handle.text($(this).slider("value"));
		},
		slide: function(event, ui) {
			draw_handle.text(ui.value);
		},
		stop: function(event, ui) {
			paint_window.data('drawwidth', ui.value);
		}
	});

	paint_window.data('drawwidth', 1);

	/* Erase width
	 */
	var erase_handle = paint_window.find('div.erasewidth div div');

	var slider = paint_window.find('div.erasewidth > div').slider({
		min: 1,
		max: 100,
		value: 10,
		create: function() {
			erase_handle.text($(this).slider("value"));
		},
		slide: function(event, ui) {
			erase_handle.text(ui.value);
		},
		stop: function(event, ui) {
			paint_window.data('erasewidth', ui.value);
		}
	});

	paint_window.data('erasewidth', 10);

	/* Draw color
	 */
	paint_window.find('div.drawcolor span').click(function() {
		paint_window.find('div.drawcolor span').css('box-shadow', '');
		$(this).css('box-shadow', '0 0 8px #0080ff');

		paint_window.data('drawcolor', $(this).css('background-color'));
		paint_window.data('colorspan', $(this));
	});

	paint_window.find('div.drawcolor span').dblclick(function() {
		paint_select_color($(this));
	});

	paint_window.find('div.drawcolor span').first().trigger('click');
}

function paint_init(paint_window, width, height) {
	paint_window.data('filename', null);
	paint_window.data('changed', false);

	var workspace = paint_window.find('div.workspace');

	workspace.empty();
	workspace.append('<div class="background" style="width:' + width + 'px; height:' + height + 'px;" />');
	workspace.append('<canvas class="picture" width="' + width + '" height="' + height + '" />');
	workspace.append('<canvas class="scratch" width="' + width + '" height="' + height + '" />');

	var paint_mode = null;

	var paint_canvas = paint_window.find('div.workspace canvas.picture');
	var paint_ctx = paint_canvas[0].getContext('2d');
	paint_ctx.lineCap = 'round';
	paint_ctx.lineJoin = 'round';

	paint_window.data('canvas', paint_canvas);
	paint_window.data('ctx', paint_ctx);

	var paint_scratch_canvas = paint_window.find('div.workspace canvas.scratch');
	var paint_scratch_ctx = paint_scratch_canvas[0].getContext('2d');
	paint_scratch_ctx.lineCap = 'round';
	paint_scratch_ctx.lineJoin = 'round';

	/* Tool buttons
	 */
	paint_window.find('div.tools button').click(function() {
		paint_window.find('div.tools button').css('border', '1px solid #000000');
		$(this).css('border', '1px solid #ff0000');
		paint_window.find('div.options > div').hide();
	});

	paint_window.find('div.tools button.draw').click(function() {
		paint_mode = PAINT_DRAW;
		paint_window.find('div.options div.drawwidth').show();
		paint_window.find('div.options div.drawcolor').show();
	});

	paint_window.find('div.tools button.erase').click(function() {
		paint_mode = PAINT_ERASE;
		paint_window.find('div.options div.erasewidth').show();
	});

	paint_window.find('div.tools button.crop').click(function() {
		paint_mode = PAINT_CROP;
	});

	paint_window.find('div.tools button.line').click(function() {
		paint_mode = PAINT_LINE;
		paint_window.find('div.options div.drawwidth').show();
		paint_window.find('div.options div.drawcolor').show();
	});

	paint_window.find('div.tools button.square').click(function() {
		paint_mode = PAINT_SQUARE;
		paint_window.find('div.options div.drawwidth').show();
		paint_window.find('div.options div.drawcolor').show();
	});

	paint_window.find('div.tools button.circle').click(function() {
		paint_mode = PAINT_CIRCLE;
		paint_window.find('div.options div.drawwidth').show();
		paint_window.find('div.options div.drawcolor').show();
	});

	paint_window.find('div.tools button.picker').click(function() {
		paint_mode = PAINT_PICKER;
		paint_window.find('div.options div.drawcolor').show();
	});

	paint_window.find('div.tools button.rotate').click(function() {
		var width = paint_canvas.width();
		var height = paint_canvas.height();
		var max = Math.max(width, height);
		var min = Math.min(width, height);

		paint_scratch_ctx.drawImage(paint_canvas[0], 0, 0);

		$('canvas.picture').attr('width', height);
		$('canvas.picture').attr('height', width);

		paint_ctx.save();
		paint_ctx.translate(min / 2, min / 2);
		paint_ctx.rotate(Math.PI / 2);
		var dy = Math.max(height - width, 0);
		paint_ctx.drawImage(paint_scratch_canvas[0], -min / 2, -min / 2 - dy);
		paint_ctx.restore();

		$('canvas.scratch').attr('width', height);
		$('canvas.scratch').attr('height', width);

		$('div.background').css('width', height + 'px');
		$('div.background').css('height', width + 'px');

		paint_window.data('changed', true);
	});


	paint_window.find('div.tools button.draw').trigger('click');

	/* Show mouse cursor
	 */
	paint_scratch_canvas.on('mousemove', function(event) {
		paint_window.data('mouse_inside', true);

		if (paint_mode == PAINT_ERASE) {
			var radius = paint_window.data('erasewidth');
			paint_scratch_ctx.strokeStyle = '#404040';
		} else if (paint_mode == PAINT_PICKER) {
			return;
		} else if (paint_mode == PAINT_CROP) {
			return;
		} else {
			var radius = paint_window.data('drawwidth');
			paint_scratch_ctx.strokeStyle = paint_window.data('drawcolor');
		}

		var window_pos = paint_window.parent().parent().position();
		var delta_x = Math.round(window_pos.left + 41);
		var delta_y = Math.round(window_pos.top + 108);

		delta_x -= Math.round($('div.workspace').scrollLeft());
		delta_y -= Math.round($('div.workspace').scrollTop());

		var mouse_x = event.clientX - delta_x;
		var mouse_y = event.clientY - delta_y;

		paint_scratch_ctx.clearRect(0, 0, paint_scratch_canvas.width(), paint_scratch_canvas.height());
		paint_scratch_ctx.beginPath();
		paint_scratch_ctx.lineWidth = 1;
		paint_scratch_ctx.arc(mouse_x, mouse_y, radius >> 1, 0, 2 * Math.PI);
		paint_scratch_ctx.stroke();
	});

	paint_scratch_canvas.on('mouseleave', function(event) {
		if (paint_mode == PAINT_CROP) {
			return;
		}

		paint_scratch_ctx.clearRect(0, 0, paint_scratch_canvas.width(), paint_scratch_canvas.height());
	});

	/* Alter picture
	 */
	workspace.on('mousedown', function(event) {
		if (event.which != 1) {
			return;
		}

		paint_window.data('changed', true);

		var window_pos = paint_window.parent().parent().position();
		var delta_x = Math.round(window_pos.left + 41);
		var delta_y = Math.round(window_pos.top + 108);

		delta_x -= Math.round($('div.workspace').scrollLeft());
		delta_y -= Math.round($('div.workspace').scrollTop());

		var mouse_x = event.clientX - delta_x;
		var mouse_y = event.clientY - delta_y;

		switch (paint_mode) {
			case PAINT_DRAW:
			case PAINT_ERASE:
				if (paint_mode == PAINT_DRAW) {
					paint_ctx.lineWidth = paint_window.data('drawwidth');
					paint_ctx.strokeStyle = paint_window.data('drawcolor');
					paint_ctx.globalCompositeOperation = 'source-over';
				} else {
					paint_ctx.lineWidth = paint_window.data('erasewidth');
					paint_ctx.globalCompositeOperation = 'destination-out';
				}

				paint_ctx.beginPath();
				paint_ctx.moveTo(mouse_x, mouse_y);

				var draw = function(event) {
					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					paint_ctx.lineTo(mouse_x, mouse_y);
					paint_ctx.stroke();
				}

				draw(event);
				workspace.on('mousemove', draw);

				workspace.on('mouseleave', function(event) {
					paint_ctx.closePath();
				});

				workspace.on('mouseenter', function(event) {
					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					paint_ctx.beginPath();
					paint_ctx.moveTo(mouse_x, mouse_y);
				});

				workspace.one('mouseup', function() {
					workspace.off('mousemove');
					workspace.off('mouseleave');
					workspace.off('mouseenter');
				});
				break;
			case PAINT_CROP:
				paint_ctx.lineWidth = paint_window.data('drawwidth');
				paint_ctx.strokeStyle = paint_window.data('drawcolor');
				paint_ctx.globalCompositeOperation = 'source-over';

				var start_x = mouse_x;
				var start_y = mouse_y;

				workspace.on('mousemove', function(event) {
					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					paint_scratch_ctx.clearRect(0, 0, paint_scratch_canvas.width(), paint_scratch_canvas.height());
					paint_scratch_ctx.strokeStyle = '#000000';
					paint_scratch_ctx.lineWidth = 1;
					paint_scratch_ctx.lineJoin = 'miter';
					paint_scratch_ctx.beginPath();
					paint_scratch_ctx.rect(start_x, start_y, mouse_x - start_x, mouse_y - start_y);
					paint_scratch_ctx.stroke();
					paint_scratch_ctx.closePath();

					paint_scratch_ctx.lineJoin = 'round';
				});

				workspace.one('mouseup', function(event) {
					workspace.off('mousemove');

					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					if (mouse_x < start_x) {
						[start_x, mouse_x] = [mouse_x, start_x];
					}

					if (mouse_y < start_y) {
						[start_y, mouse_y] = [mouse_y, start_y];
					}

					var old_width = paint_canvas.width();
					var old_height = paint_canvas.height();
					var new_width = mouse_x - start_x;
					var new_height =  mouse_y - start_y;

					var image_data = paint_ctx.getImageData(0, 0, old_width, old_height);
					paint_scratch_ctx.putImageData(image_data, 0, 0);

					$('canvas.picture').attr('width', new_width);
					$('canvas.picture').attr('height', new_height);

					image_data = paint_scratch_ctx.getImageData(start_x, start_y, new_width, new_height);
					paint_ctx.putImageData(image_data, 0, 0);
					paint_scratch_ctx.clearRect(0, 0, old_width, old_height);

					$('canvas.scratch').attr('width', new_width);
					$('canvas.scratch').attr('height', new_height);

					$('div.background').css('width', new_width + 'px');
					$('div.background').css('height', new_height + 'px');
				});
				break;
			case PAINT_LINE:
				paint_ctx.lineWidth = paint_window.data('drawwidth');
				paint_ctx.strokeStyle = paint_window.data('drawcolor');
				paint_ctx.globalCompositeOperation = 'source-over';

				var start_x = mouse_x;
				var start_y = mouse_y;

				workspace.on('mousemove', function(event) {
					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					paint_scratch_ctx.clearRect(0, 0, paint_scratch_canvas.width(), paint_scratch_canvas.height());
					paint_scratch_ctx.strokeStyle = paint_ctx.strokeStyle;
					paint_scratch_ctx.lineWidth = paint_ctx.lineWidth;
					paint_scratch_ctx.beginPath();
					paint_scratch_ctx.moveTo(start_x, start_y);
					paint_scratch_ctx.lineTo(mouse_x, mouse_y);
					paint_scratch_ctx.stroke();
				});

				workspace.one('mouseup', function(event) {
					workspace.off('mousemove');

					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					paint_ctx.beginPath();
					paint_ctx.moveTo(start_x, start_y);
					paint_ctx.lineTo(mouse_x, mouse_y);
					paint_ctx.stroke();

					paint_scratch_ctx.clearRect(0, 0, paint_scratch_canvas.width(), paint_scratch_canvas.height());
				});
				break;
			case PAINT_SQUARE:
				paint_ctx.lineWidth = paint_window.data('drawwidth');
				paint_ctx.strokeStyle = paint_window.data('drawcolor');
				paint_ctx.globalCompositeOperation = 'source-over';

				var start_x = mouse_x;
				var start_y = mouse_y;

				workspace.on('mousemove', function(event) {
					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					paint_scratch_ctx.clearRect(0, 0, paint_scratch_canvas.width(), paint_scratch_canvas.height());
					paint_scratch_ctx.strokeStyle = paint_ctx.strokeStyle;
					paint_scratch_ctx.lineWidth = paint_ctx.lineWidth;
					paint_scratch_ctx.lineJoin = 'miter';
					paint_scratch_ctx.beginPath();
					paint_scratch_ctx.rect(start_x, start_y, mouse_x - start_x, mouse_y - start_y);
					paint_scratch_ctx.stroke();
					paint_scratch_ctx.closePath();

					paint_scratch_ctx.lineJoin = 'round';
				});

				workspace.one('mouseup', function(event) {
					workspace.off('mousemove');

					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					paint_ctx.lineJoin = 'miter';
					paint_ctx.beginPath();
					paint_ctx.rect(start_x, start_y, mouse_x - start_x, mouse_y - start_y);
					paint_ctx.stroke();
					paint_ctx.closePath();

					paint_ctx.lineJoin = 'round';

					paint_scratch_ctx.clearRect(0, 0, paint_scratch_canvas.width(), paint_scratch_canvas.height());
				});
				break;
			case PAINT_CIRCLE:
				paint_ctx.lineWidth = paint_window.data('drawwidth');
				paint_ctx.strokeStyle = paint_window.data('drawcolor');
				paint_ctx.globalCompositeOperation = 'source-over';

				var start_x = mouse_x;
				var start_y = mouse_y;

				var draw_circle = function(ctx, x, y, rx, ry) {
					rx /= 2;
					ry /= 2;
					x += rx;
					y += ry;

					ctx.beginPath();
					ctx.moveTo(x + rx, y);

					var step = 2 * Math.PI / Math.max(rx, ry, 12);
					for (var a = 0; a <= 2 * Math.PI + step; a += step) {
						var sx = Math.cos(a) * rx;
						var sy = Math.sin(a) * ry;
						ctx.lineTo(x + sx, y + sy);
					}

					ctx.stroke();
					ctx.closePath();
				};

				workspace.on('mousemove', function(event) {
					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					paint_scratch_ctx.clearRect(0, 0, paint_scratch_canvas.width(), paint_scratch_canvas.height());
					paint_scratch_ctx.strokeStyle = paint_ctx.strokeStyle;
					paint_scratch_ctx.lineWidth = paint_ctx.lineWidth;
					draw_circle(paint_scratch_ctx, start_x, start_y, mouse_x - start_x, mouse_y - start_y);
				});

				workspace.one('mouseup', function(event) {
					workspace.off('mousemove');

					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					draw_circle(paint_ctx, start_x, start_y, mouse_x - start_x, mouse_y - start_y);

					paint_scratch_ctx.clearRect(0, 0, paint_scratch_canvas.width(), paint_scratch_canvas.height());
				});
				break;
			case PAINT_PICKER:
				var picker = function(event) {
					mouse_x = event.clientX - delta_x;
					mouse_y = event.clientY - delta_y;

					var color = paint_ctx.getImageData(mouse_x, mouse_y, 1, 1).data;
					var rgb = 'rgb(' + color.join(', ') + ')';
					rgb = paint_rgb_to_hex(rgb);
					var span = paint_window.data('colorspan');
					span.css('background-color', rgb);
				}

				picker(event);
				workspace.on('mousemove', picker);

				workspace.one('mouseleave', function(event) {
					workspace.off('mousemove', picker);
				});

				workspace.one('mouseup', function(event) {
					workspace.off('mousemove', picker);
				});
				break;
		}
	});

	/* Key press
	 */
	paint_window.parent().parent().on('keydown', function(event) {
		switch (event.key) {
			case 'Escape':
				break;
			case 'd':
				paint_window.find('div.tools button.draw').trigger('click');
				break;
			case 'e':
				paint_window.find('div.tools button.erase').trigger('click');
				break;
			case 'l':
				paint_window.find('div.tools button.line').trigger('click');
				break;
			case 'p':
				paint_window.find('div.tools button.picker').trigger('click');
				break;
			case 's':
				paint_window.find('div.tools button.square').trigger('click');
				break;
			case 'c':
				paint_window.find('div.tools button.circle').trigger('click');
				break;
			default:
				return;
		}

		workspace.off('mousemove');
		workspace.off('mouseup');
		workspace.off('mouseleave');
		workspace.off('mouseenter');

		paint_scratch_ctx.clearRect(0, 0, paint_scratch_canvas.width(), paint_scratch_canvas.height());
	});

	return paint_ctx;
}

function paint_load_file(paint_window, filename, is_autosave = false) {
	var img = $('<img />');

	img.one('load', function() {
		var ctx = paint_init(paint_window, $(this)[0].width, $(this)[0].height);
		ctx.drawImage(img[0], 0, 0);
		delete img;

		if (is_autosave == false) {
			paint_window.data('filename', filename);
			paint_window.set_header(filename);
		} else {
			paint_window.data('changed', true);
		}
	});

	img.attr('src', orb_download_url(filename));
}

function paint_save_file(paint_window, filename, is_autosave = false) {
	var extension = orb_file_extension(filename);

	if (extension == 'jpg') {
		extension = 'jpeg';
	}

	var canvas = paint_window.data('canvas');
	var content = canvas[0].toDataURL('image/' + extension);

	if (content.substr(0, 5) != 'data:') {
		return false;
	}

	var comma = content.indexOf(',');
	if (comma == -1) {
		return false;
	}

	var info = content.substring(5, comma).split(';');
	if (info[1] != 'base64') {
		return false;
	}

	content = content.substr(comma + 1);
	content = atob(content);

	orb_file_save(filename, content, true, function() {
		if (is_autosave == false) {
			paint_window.data('changed', false);
			paint_window.data('filename', filename);
			paint_window.set_header(filename);
		}
	}, function() {
		if (is_autosave == false) {
			orb_alert('Error while saving file.', 'Error');
		}
	});
}

function paint_menu_click(paint_window, item) {
	var canvas = paint_window.data('canvas');

	switch (item) {
		case 'New':
			if (paint_dont_discard(paint_window)) {
				return false;
			}

			var new_content =
				'<div class="paint_new">' +
				'<label>Width:</label>' +
				'<input type="text" value="600" class="form-control" />' +
				'<label>Height:</label>' +
				'<input type="text" value="400" class="form-control" />' +
				'<div class="btn-group">' +
				'<button class="btn btn-default">Create</button>' +
				'</div>' +
				'</div>';

			var new_window = $(new_content).orb_window({
				header:'New picture',
				width: 300,
				height: 170,
				maximize: false,
				minimize: false,
				resize: false,
				dialog: true
			});

			new_window.find('button').click(function() {
				var width = parseInt(new_window.find('input:nth-of-type(1)').val())
				var height = parseInt(new_window.find('input:nth-of-type(2)').val());

				if ((isNaN(width) == false) && (isNaN(height) == false)) {
					if ((width > 0) && (height > 0)) {
						paint_init(paint_window, width, height);
						new_window.close();
					}
				}
			});

			new_window.open();
			break;
		case 'Open':
			if (paint_dont_discard(paint_window)) {
				return false;
			}

			orb_file_dialog('Open', function(filename) {
				if (paint_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					paint_load_file(paint_window, filename);
				}
			}, 'Pictures');
			break;
		case 'Save':
			var filename = paint_window.data('filename');
			if (filename != undefined) {
				paint_save_file(paint_window, filename);
				break;
			}
		case 'Save as':
			var filename = paint_window.data('filename');
			if (filename == undefined) {
				var start_dir = 'Pictures';
				var start_file = undefined;
			} else {
				var start_dir = orb_file_dirname(filename);
				var start_file = orb_file_filename(filename);
			}

			orb_file_dialog('Save', function(filename) {
				if (paint_valid_extension(filename) == false) {
					filename += '.png';
				}

				if (filename != paint_window.data('filename')) {
					orb_file_exists(filename, function(exists) {
						if (exists) {
							if (confirm('File already exists. Overwrite?') == false) {
								return;
							}
						}
						paint_save_file(paint_window, filename);
					}, function() {
						orb_alert('Error while saving file.', 'Error');
					});
				} else {
					paint_save_file(paint_window, filename);
				}
			}, start_dir, start_file);
			break;
		case 'Resize':
			var width = paint_window.find('canvas.picture').attr('width');
			var height = paint_window.find('canvas.picture').attr('height');

			var resize_form =
				'<div class="paint_resize">' +
				'<label for="width">Picture width:</label>' +
				'<input type="text" value="' + width + '" class="form-control" />' +
				'<label for="height">Picture height:</label>' +
				'<input type="text" value="' + height + '"class="form-control" />' +
				'<div class="btn-group">' +
				'<button class="btn btn-default">Resize</button>' +
				'<button class="btn btn-default cancel">Cancel</button>' +
				'</div>' +
				'</div>';

			var resize_window = $(resize_form).orb_window({
				header:'Resize picture',
				width: 300,
				height: 170,
				maximize: false,
				minimize: false,
				resize: false,
				dialog: true,
				close: function() {
					$(document).off('keydown', key_handler);
				}
			});

			resize_window.find('input').first().on('change', function() {
				var temp_width = parseInt(resize_window.find('input').first().val());

				if (temp_width < 1) {
					temp_width = 1;
					resize_window.find('input').first().val(temp_width);
				}

				var temp_height = Math.round(height / width * temp_width);

				resize_window.find('input').last().val(temp_height);
			});

			resize_window.find('input').last().on('change', function() {
				if (parseInt(resize_window.find('input').last().val()) < 1) {
					resize_window.find('input').last().val(1);
				}
			});

			resize_window.find('button').first().click(function() {
				var new_width = resize_window.find('input').first().val();
				var new_height = resize_window.find('input').last().val();

				resize_window.close();

				var paint_canvas = paint_window.data('canvas');
				var paint_ctx = paint_canvas[0].getContext('2d');

				var temp_canvas = $('<canvas width="' + width + '" height="' + height + '" />');
				var temp_ctx = temp_canvas[0].getContext('2d');

				temp_ctx.drawImage(paint_canvas[0], 0, 0);

				paint_window.find('div.background').css('width', new_width + 'px');
				paint_window.find('div.background').css('height', new_height + 'px');

				paint_window.find('canvas.picture').attr('width', new_width);
				paint_window.find('canvas.picture').attr('height', new_height);

				paint_window.find('canvas.scratch').attr('width', new_width);
				paint_window.find('canvas.scratch').attr('height', new_height);

				paint_ctx.drawImage(temp_canvas[0], 0, 0, new_width, new_height);
			});

			resize_window.find('button').last().click(function() {
				resize_window.close();
			});

			resize_window.open();

			var key_handler = function(event) {
				if (event.which == 27) {
					resize_window.find('div.btn-group button.cancel').trigger('click');
				}
			};
			$(document).on('keydown', key_handler);
			break;
		case 'Exit':
			paint_window.close();
			break;
		case 'About':
			orb_alert('<img src="' + PAINT_ICON + '" class="about" draggable="false" />Paint\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function paint_open_icon(icon) {
	var filename = orb_icon_to_filename(icon);

	if (filename == undefined) {
		return;
	}

	paint_open(filename);
}

function paint_open(filename = undefined) {
	var window_content =
		'<div class="paint">' +
		'<div class="options">' +
		'<div class="drawwidth"><div><div class="ui-slider-handle"></div></div></div>' +
		'<div class="erasewidth"><div><div class="ui-slider-handle"></div></div></div>' +
		'<div class="drawcolor">' +
		'<span style="background-color:#000000"></span>' +
		'<span style="background-color:#808080"></span>' +
		'<span style="background-color:#ffffff"></span>' +
		'<span style="background-color:#804000"></span>' +
		'<span style="background-color:#ff0000"></span>' +
		'<span style="background-color:#ff8000"></span>' +
		'<span style="background-color:#ffff00"></span>' +
		'<span style="background-color:#00ff00"></span>' +
		'<span style="background-color:#008000"></span>' +
		'<span style="background-color:#00ffff"></span>' +
		'<span style="background-color:#0000ff"></span>' +
		'<span style="background-color:#000080"></span>' +
		'<span style="background-color:#ff00ff"></span>' +
		'<span style="background-color:#800080"></span>' +
		'</div>' +
		'</div>' +
		'<div class="tools">' +
		'<button class="tool draw"><img src="/apps/paint/icons/draw.png" title="Draw" draggable="false" /></button>' +
		'<button class="tool erase"><img src="/apps/paint/icons/erase.png" title="Erase" draggable="false" /></button>' +
		'<button class="tool crop"><img src="/apps/paint/icons/crop.png" title="Crop" draggable="false" /></button>' +
		'<button class="tool line"><img src="/apps/paint/icons/line.png" title="Line" draggable="false" /></button>' +
		'<button class="tool square"><img src="/apps/paint/icons/square.png" title="Square" draggable="false" /></button>' +
		'<button class="tool circle"><img src="/apps/paint/icons/circle.png" title="Circle" draggable="false" /></button>' +
		'<button class="tool picker"><img src="/apps/paint/icons/picker.png" title="Picker" draggable="false" /></button>' +
		'<button class="tool rotate"><img src="/apps/paint/icons/rotate.png" title="Rotate" draggable="false" /></button>' +
		'</div>' +
		'<div class="workspace"></div>' +
		'</div>';

	var paint_window = $(window_content).orb_window({
		header:'Paint',
		icon: PAINT_ICON,
		width: 700,
		height: 500,
		minWidth: 600,
		menu: {
			'File':  [ 'New', 'Open', 'Save', 'Save as', '-', 'Exit' ],
			'Tools': [ 'Resize' ],
			'Help':  [ 'About' ]
		},
		menuCallback: paint_menu_click,
		close: function() {
			if (paint_dont_discard(paint_window)) {
				return false;
			}

            clearInterval(autosave);
            orb_file_remove(autosave_filename);
		}
	});

	var height = 117 + (paint_window.find('div.tools button').length * 32);
	paint_window.parent().parent().css('min-height', height.toString() + 'px');

	var autosave_base = 'Temporary/paint_autosave_';
	if (filename != undefined) {
		var autosave_load = (filename.substr(0, autosave_base.length) == autosave_base);
	} else {
		var autosave_load = false;
	}

	if (autosave_load) {
		var autosave_filename = filename;
	} else {
		var autosave_filename = autosave_base + Date.now() + '.png';
	}

	paint_init_options(paint_window);

	if (filename != undefined) {
		paint_load_file(paint_window, filename, autosave_load);
	} else {
		paint_init(paint_window, 600, 400);
	}

	paint_window.open();

	var autosave = setInterval(function() {
		if (paint_window.data('changed') == false) {
			return;
		}

		paint_save_file(paint_window, autosave_filename, true);
	}, 5000);
}

$(document).ready(function() {
	orb_startmenu_add('Paint', PAINT_ICON, paint_open);

	picture_valid_extensions.forEach(function(extension) {
		orb_contextmenu_extra_item(extension, 'Edit with Paint', 'paint-brush', paint_open_icon);
	});
});
