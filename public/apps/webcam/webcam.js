/* Orb Webcam application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const WEBCAM_ICON = '/apps/webcam/webcam.png';

var webcam_started = false;

function webcam_menu_click(webcam_window, item) {
	switch (item) {
		case 'Capture image':
			var video = webcam_window.find('video')[0];

			var canvas = document.createElement('canvas');
			canvas.width = video.videoWidth;
			canvas.height = video.videoHeight;

			var ctx = canvas.getContext('2d');
			ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

			var image = canvas.toDataURL('image/jpeg');
			if (image.substr(0, 5) != 'data:') {
				return false;
			}

			var comma = image.indexOf(',');
			if (comma == -1) {
				return false;
			}

			var info = image.substring(5, comma).split(';');
			if (info[1] != 'base64') {
				return false;
			}

			image = image.substr(comma + 1);
			image = atob(image);

			orb_file_dialog('Save', function(filename) {
				orb_file_save(filename, image, true, undefined, function() {
					orb_alert('Error while saving image.', 'Error');
				});
			}, '', 'webcam.jpg');
			break;
		case 'Exit':
			webcam_window.close();
			break;
		case 'About':
			orb_alert('<img src="' + WEBCAM_ICON + '" class="about" draggable="false" />Webcam\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function webcam_start(webcam_window) {
	var webcams = navigator.mediaDevices;

	webcams.getUserMedia({
		audio: false,
		video: true
	}).then(function(vidStream) {
		var video = webcam_window.find('video')[0];

		if ('srcObject' in video) {
			video.srcObject = vidStream;
		} else {
			video.src = window.URL.createObjectURL(vidStream);
		}

		video.onloadedmetadata = function() {
			video.play();
		};

		webcam_started = true;
	}).catch(function() {
		webcam_window.close();
		orb_alert('No webcam found or no access to webcam.', 'Webcam error');
	});
}

function webcam_stop(webcam_window) {
	var video = webcam_window.find('video')[0];
	if (video.srcObject == null) {
		return;
	}

	var tracks = video.srcObject.getTracks();

	tracks.forEach((track) => {
		track.stop();
	});
}

function webcam_open(filename = undefined) {
	if (webcam_started) {
		orb_alert('Webcam already running.');
		return;
	}

	var window_content =
		'<div class="webcam">' +
		'<video></video>' +
		'</div>';

	var webcam_window = $(window_content).orb_window({
		header:'Webcam',
		icon: WEBCAM_ICON,
		width: 500,
		height: 390,
		menu: {
			'File': [ 'Capture image', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: webcam_menu_click,
		close: function() {
			webcam_stop(webcam_window);
			webcam_started = false;
		}
	});

	webcam_start(webcam_window);

	webcam_window.open();
}

$(document).ready(function() {
	orb_startmenu_add('Webcam', WEBCAM_ICON, webcam_open);
});
