/* Orb Sticky Note application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const STICKYNOTE_ICON = '/apps/stickynote/stickynote.png';

const STICKYNOTE_SIZE = 200;

function stickynote_save(note) {
	var data = {
		x: note.data('x'),
		y: note.data('y'),
		text: note.text()
	};

	var index = note.data('index');
	if (index != undefined) {
		data['index'] = index;
	}

	$.post('/stickynote/save', data).done(function(data) {
		note.data('index', $(data).find('index').text());
	});
}

function stickynote_edit(note) {
	if (note.find('textarea').length > 0) {
		return;
	}

	var text = note.text();

	var editor = $('<textarea>' + text + '</textarea>');
	editor.on('blur', function() {
		var text = $(this).val();
		note.empty();
		note.text(text);
		note.css('padding', '5px');

		stickynote_save(note);

		note.css('z-index', 1);
	});

	var zindex = orb_window_max_zindex() + 1;
	note.css('z-index', zindex);

	note.empty();
	note.append(editor);
	note.css('padding', '3px');

	editor.focus();

	var elem = editor[0];
	elem.setSelectionRange(text.length, text.length);
}

function stickynote_delete(note) {
	var delete_note = function() {
		$.post('/stickynote/delete', { index: note.data('index') });
	}

	if (note.data('index') == undefined) {
		note.remove();
	} else if (note.text() == '') {
		delete_note();
		note.remove();
	} else {
		orb_confirm('Delete this note?', function() {
			delete_note();
			note.remove();
		});
	}
}

function stickynote_contextmenu_handler(note, option) {
	if (option == 'Delete this note') {
		stickynote_delete(note);
	} else if (option == 'About') {
		orb_alert('<img src="' + STICKYNOTE_ICON + '" class="about" draggable="false" />Sticky Note\nCopyright (c) by Hugo Leisink', 'About');
	}
}

function stickynote_fix_xy(x, y) {
	var windows_width = Math.round($('div.windows').width());
	var windows_height = Math.round($('div.windows').height());

	if (x < 0) {
		x = windows_width + x - STICKYNOTE_SIZE;
	} else if (x + STICKYNOTE_SIZE >= windows_width) {
		x = windows_width - STICKYNOTE_SIZE;
	}

	if (y < 0) {
		y = windows_height + y - STICKYNOTE_SIZE;
	} else if (y + STICKYNOTE_SIZE >= windows_height) {
		y = windows_height - STICKYNOTE_SIZE;
	}

	if (x < 0) {
		x = 0;
	}

	if (y < 0) {
		y = 0;
	}

	return [x, y];
}

function stickynote_set_xy(note) {
	var windows_width = Math.round($('div.windows').width());
	var windows_height = Math.round($('div.windows').height());

	var pos = note.position();
	var x = Math.round(pos.left);
	var y = Math.round(pos.top);

	if (x > (windows_width / 2)) {
		x -= windows_width - STICKYNOTE_SIZE;
	}

	if (y > (windows_height / 2)) {
		y -= windows_height - STICKYNOTE_SIZE;
	}

	note.data('x', x);
	note.data('y', y);
}

function stickynote_create(x, y, text, index = null) {
	var note = $('<div class="stickynote">' + text + '</div>');

	$('div.windows').append(note);

	if (index != null) {
		note.data('index', index);
	}

	note.css('left', x + 'px');
	note.css('top', y + 'px');
	stickynote_set_xy(note);

    var zindex = orb_window_max_zindex() + 1;
	note.css('z-index', zindex);

	note.on('dblclick', function() {
		stickynote_edit(note);
	});

	if ($('div.desktop').attr('mobile') == 'yes') {
		note.on('click', function() {
			stickynote_edit(note);
		});
	}

	note.on('contextmenu', function(event) {
		menu_entries = [
			{ name: 'Delete this note', icon: 'remove' },
			{ name: 'About', icon: 'info-circle' }
		];
		orb_contextmenu_show($(this), event, menu_entries, stickynote_contextmenu_handler);

		return false;
	});

	note.draggable({
		containment: 'div.windows',
		stop: function() {
			var pos = $(this).position();
			if (pos.left < 0) {
				$(this).css('left', '0px');
			}
			if (pos.top < 0) {
				$(this).css('top', '0px');
			}

			stickynote_set_xy($(this));
			stickynote_save($(this));

			if ($('div.desktop').attr('mobile') == 'yes') {
				if ((pos.left == 0) && (pos.top == 0)) {
					stickynote_delete($(this));
				}
			}
		}
	});
}

function stickynote_open(filename = undefined) {
	var x = Math.round((window.innerWidth / 2) - (STICKYNOTE_SIZE / 2));
	x += Math.floor((Math.random() * 50) - 25);
	if (x < 0) {
		x = 0;
	}

	var y = Math.round((window.innerHeight / 2.5) - (STICKYNOTE_SIZE / 2));
	y += Math.floor((Math.random() * 50) - 25);
	if (y < 0) {
		y = 0;
	}

	stickynote_create(x, y, '');
}

$(document).ready(function() {
	orb_startmenu_add('Sticky Note', STICKYNOTE_ICON, stickynote_open);

	$.ajax('/stickynote/load').done(function(data) {
		$(data).find('note').each(function() {
			var x = parseInt($(this).find('x').text());
			var y = parseInt($(this).find('y').text());
			var text = $(this).find('text').text();
			var index = $(this).find('index').text();

			[x, y] = stickynote_fix_xy(x, y);

			stickynote_create(x, y, text, index);
		});
	});

	var resize = 0;
	$(window).on('resize', function() {
		var current = ++resize;
		setTimeout(function() {
			if (current != resize) {
				return;
			}

			var windows_width = Math.round($('div.windows').width());
			var windows_height = Math.round($('div.windows').height());

			$('div.windows div.stickynote').each(function() {
				var pos = $(this).position();

				var x = $(this).data('x');
				var y = $(this).data('y');

				x = (x == undefined) ? pos.left : parseInt(x);
				y = (y == undefined) ? pos.top : parseInt(y);

				[x, y] = stickynote_fix_xy(x, y);

				$(this).css('left', x.toString() + 'px');
				$(this).css('top', y.toString() + 'px');

				stickynote_set_xy($(this));
				stickynote_save($(this));
            });
        }, 100);
	});
});
