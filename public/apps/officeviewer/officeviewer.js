/* Orb Office Viewer application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const OFFICEVIEWER_ICON = '/apps/officeviewer/officeviewer.png';

const OFFICEVIEWER_EXTENSION = [ 'docx', 'odt', 'ods', 'odp' ];

function officeviewer_open_file(officeviewer_window, filename) {
	officeviewer_window.find('div.document').empty();

	var extension = orb_file_extension(filename);

	if (extension == 'docx') {
		// Microsoft Word
		orb_file_open(filename, function(docx_data) {
			var container = officeviewer_window.find('div.document')[0];
			docx.renderAsync(docx_data, container);
		});
	} else {
		// OpenDocument
		var container = officeviewer_window.find('div.document')[0];
		odfcanvas = new odf.OdfCanvas(container);
		odfcanvas.load(orb_download_url(filename));
	}
}

function officeviewer_menu_click(officeviewer_window, item) {
	switch (item) {
		case 'Open file':
			orb_file_dialog('Open', function(filename) {
				var extension = orb_file_extension(filename);
				if (OFFICEVIEWER_EXTENSION.includes(extension) == false) {
					orb_alert('Invalid file extension.');
				} else {
					officeviewer_open_file(officeviewer_window, filename);
				}
			}, 'Documents');
			break;
		case 'Exit':
			officeviewer_window.close();
			break;
		case 'About':
			var message = [
				'<img src="' + OFFICEVIEWER_ICON + '" class="about" draggable="false" />Office Viewer',
				'Copyright (c) by Hugo Leisink\n',
				'Webodf.js',
				'Copyright (c) by KO GmbH',
				'<a href="https://webodf.org/" target="_blank">https://webodf.org/</a>\n',
				'docx-preview',
				'Copyright (c) by Volodymyr Baydalka',
				'<a href="https://github.com/VolodymyrBaydalka/docxjs" target="_blank">https://github.com/VolodymyrBaydalka/docxjs</a>'
			];
			orb_alert(message.join('\n'), 'About');
			break;
	}
}

function officeviewer_open(filename = undefined) {
	var officeviewer_content = '<div class="officeviewer"><div class="document"></div></div>';
	
	var officeviewer_window = $(officeviewer_content).orb_window({
		header: 'Office Viewer',
		icon: OFFICEVIEWER_ICON,
		width: 830,
		height: 500,
		menu: {
			'File': [ 'Open file', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: officeviewer_menu_click
	});
	
	orb_load_javascript('/apps/officeviewer/polyfill.min.js');
	orb_load_javascript('/apps/officeviewer/jszip.min.js');
	orb_load_javascript('/apps/officeviewer/docx-preview.min.js');

	orb_load_javascript('/apps/officeviewer/webodf.js');

	if (filename != undefined) {
		officeviewer_open_file(officeviewer_window, filename);
	}

	officeviewer_window.open();
}

$(document).ready(function() {
	orb_startmenu_add('Office Viewer', OFFICEVIEWER_ICON, officeviewer_open);

	OFFICEVIEWER_EXTENSION.forEach(function(extension) {
		orb_upon_file_open(extension, officeviewer_open, '/images/icons/' + extension + '.png');
	});
});
