/* Orb Settings application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const SETTINGS_ICON = '/apps/settings/settings.png';

var settings_windows_open = [];
var settings_default_color = null;
var settings_wallpaper_extensions = [ 'gif', 'jpeg', 'jpg', 'png', 'webp' ];

/* Colors
 */
function settings_color() {
	if (settings_windows_open.includes('color')) {
		return;
	}

	var window_content =
		'<div class="color">' +
		'<form><input type="text" id="color" name="color" value="' +
		settings_default_color + '" /></form>' +
		'<div id="colorpicker"></div>' +
		'<div class="btn-group">' +
		'<button class="btn btn-default save">Save</button>' +
		'<button class="btn btn-default default">Default</button>' +
		'</div>' +
		'</div>';

	var color_window = $(window_content).orb_window({
		header:'Color',
		icon: '/apps/settings/color.png',
		width: 220,
		height: 270,
		maximize: false,
		minimize: false,
		resize: false,
		close: function() {
			settings_windows_open = array_remove(settings_windows_open, 'color');
		}
	});

	orb_load_javascript('/apps/settings/color/farbtastic.js');
	orb_load_stylesheet('/apps/settings/color/farbtastic.css');

	var farbtastic = color_window.find('#colorpicker').farbtastic('input#color');

	orb_setting_get('system/color', function(color) {
		jQuery.farbtastic('#colorpicker').setColor(color);
	}, function() {
		orb_alert('Error loading color.', 'Error');
	});

	color_window.find('button.save').click(function() {
		var color = color_window.find('input#color').val();
		orb_setting_set('system/color', color, function() {
			orb_window_set_color(color);
			color_window.close();
		}, function() {
			orb_alert('Error setting color.', 'Error');
		});
	});

	color_window.find('button.default').click(function() {
		jQuery.farbtastic('#colorpicker').setColor(settings_default_color);
	});

	color_window.open();

	settings_windows_open.push('color');
}

/* Zoom for mobile devices
 */
function settings_zoom() {
	if (settings_windows_open.includes('zoom')) {
		return;
	}

	var window_content =
		'<div class="zoom">' +
		'<label>Zoom for mobile devices:</label>' +
		'<div id="slider"></div>' +
		'<div class="btn-group">' +
		'<button class="btn btn-default" type="button">Set zoom</button>' +
		'</div>' +
		'</div>';

	var zoom_window = $(window_content).orb_window({
		header:'Mobile zoom',
		icon: '/apps/settings/zoom.png',
		width: 500,
		height: 100,
		maximize: false,
		minimize: false,
		resize: false,
		close: function() {
			settings_windows_open = array_remove(settings_windows_open, 'zoom');
		}
	});

	zoom_window.find('div#slider').slider({
		min: 0.5,
		max: 1,
		step: 0.05,
		create: function() {
			zoom_window.find('div#slider span').text($(this).slider('value'));
		},
		slide: function(event, ui) {
			zoom_window.find('div#slider span').text(ui.value);
		},
	});

	orb_setting_get('system/zoom', function(zoom) {
		zoom_window.find('div#slider').slider('option', 'value', zoom);
		zoom_window.find('div#slider span').text(zoom);
	}, function() {
		orb_alert('Error loading mobile zoom factor.', 'Error');
	});

	zoom_window.find('button').click(function() {
		var zoom = zoom_window.find('div#slider').slider('value');
		orb_setting_set('system/zoom', zoom, function() {
			zoom_window.close();
			if ($('div.desktop').attr('mobile') == 'yes') {
				var content = 'width=device-width, initial-scale=' + zoom + ', maximum-scale=' + zoom
				$('head meta[name=viewport]').attr('content', content);
			}
		}, function() {
			orb_alert('Error setting mobile zoom factor.', 'Error');
		});
	});

	zoom_window.open();

	settings_windows_open.push('zoom');
}

/* Password
 */
function settings_password() {
	if (settings_windows_open.includes('password')) {
		return;
	}

	var window_content =
		'<div class="password">' +
		'<div class="form-group">' +
		'<label>Current password:</label>' +
		'<input type="password" class="current form-control">' +
		'</div><div class="form-group">' +
		'<label>New password:</label>' +
		'<input type="password" class="new form-control">' +
		'</div><div class="form-group">' +
		'<label>Repeat new password:</label>' +
		'<input type="password" class="repeat form-control">' +
		'</div><div class="btn-group">' +
		'<button class="btn btn-default">Change password</button>' +
		'</div>' +
		'</div>';

	var password_window = $(window_content).orb_window({
		header:'Password',
		icon: '/apps/settings/password.png',
		width: 400,
		height: 260,
		maximize: false,
		minimize: false,
		resize: false,
		open: function() {
			password_window.find('input').first().focus();
		},
		close: function() {
			settings_windows_open = array_remove(settings_windows_open, 'password');
		}
	});

	password_window.find('button').click(function() {
		var current = password_window.find('input.current').val();
		var newpwd = password_window.find('input.new').val();
		var repeat = password_window.find('input.repeat').val();

		$.post('/settings/password', {
			current: current,
			newpwd: newpwd,
			repeat: repeat
		}).done(function() {
			password_window.close();
			orb_alert('Password has been changed.');
		}).fail(function(result) {
			orb_alert($(result.responseText).find('result').text());
		});
	});

	password_window.open();

	settings_windows_open.push('password');
}

/* Wallpaper
 */
function settings_wallpaper() {
	if (settings_windows_open.includes('wallpaper')) {
		return;
	}

	var window_content =
		'<div class="wallpaper">' +
		'<label>Wallpaper:</label>' +
		'<div class="input-group">' +
		'<input type="text" readonly="readonly" class="form-control">' +
		'<span class="input-group-btn">' +
		'<button class="btn btn-default" type="button">Select wallpaper</button>' +
		'</span>' +
		'</div>' +
		'</div>';

	var wallpaper_window = $(window_content).orb_window({
		header:'Wallpaper',
		icon: '/apps/settings/wallpaper.png',
		width: 500,
		height: 100,
		maximize: false,
		minimize: false,
		resize: false,
		close: function() {
			settings_windows_open = array_remove(settings_windows_open, 'wallpaper');
		}
	});

	orb_setting_get('system/wallpaper', function(wallpaper) {
		wallpaper_window.find('input').val(wallpaper);
	}, function() {
		orb_alert('Error loading wallpaper.', 'Error');
	});

	wallpaper_window.find('button').click(function() {
		orb_file_dialog('Select', function(wallpaper) {
			var extension = orb_file_extension(wallpaper);

			if (extension != false) {
				extension = extension.toLowerCase();
			}

			if (settings_wallpaper_extensions.includes(extension) == false) {
				orb_alert('Invalid wallpaper file.');
			} else {
				orb_setting_set('system/wallpaper', wallpaper, function() {
					wallpaper_window.find('input').val(wallpaper);
					orb_desktop_load_wallpaper(wallpaper);
				}, function() {
					orb_alert('Error setting wallpaper.', 'Error');
				});
			}
		}, 'Pictures');
	});

	wallpaper_window.open();

	settings_windows_open.push('wallpaper');
}

/* Settings
 */
function settings_add_section(settings_window, icon_label, icon_image, callback) {
	var icon = orb_make_icon(icon_label, '/apps/settings/' + icon_image);
	var section = $(icon);
	section.find('img').attr('draggable', 'false');
	section.dblclick(callback);

	settings_window.append(section);
}

function settings_open(filename = undefined) {
	if (settings_windows_open.includes('settings')) {
		return;
	}

	$.ajax({
		url: '/settings/color'
	}).done(function(data) {
		settings_default_color = $(data).find('color').text();
	});

	var window_content =
		'<div class="settings">' +
		'</div>';

	var settings_window = $(window_content).orb_window({
		header:'Settings',
		icon: SETTINGS_ICON,
		width: 350,
		height: 100,
		maximize: false,
		resize: false,
		close: function() {
			settings_windows_open = array_remove(settings_windows_open, 'settings');
		}
	});

	settings_add_section(settings_window, 'Color', 'color.png', settings_color);
	settings_add_section(settings_window, 'Mobile zoom', 'zoom.png', settings_zoom);
	if ($('div.desktop').attr('login') != 'none') {
		settings_add_section(settings_window, 'Password', 'password.png', settings_password);
	}
	settings_add_section(settings_window, 'Wallpaper', 'wallpaper.png', settings_wallpaper);

	settings_window.open();

	settings_windows_open.push('settings');
}

$(document).ready(function() {
	orb_startmenu_system('Settings', SETTINGS_ICON, settings_open);
});
