/* Orb Notepad application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const NOTEPAD_ICON = '/apps/notepad/notepad.png';

function notepad_focus(notepad_window, position = null) {
	var textarea = notepad_window.find('textarea');
	textarea.focus();

	if (position != null) {
		textarea[0].setSelectionRange(position, position);
	}
}

function notepad_dont_discard(notepad_window) {
	if (notepad_window.data('changed')) {
		if (confirm('File has been changed. Discard?') == false) {
			return true;
		}
	}

	return false;
}

function notepad_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return true;
	}

	var extensions = [ 'txt', 'ots', 'log', 'conf', 'html', 'csv', ORB_NO_EXTENSION ];

	return extensions.includes(extension.toLowerCase());
}

function notepad_save_file(notepad_window, filename, is_autosave = false) {
	var content = notepad_window.find('textarea').val();

	orb_file_save(filename, content, false, function() {
		if (is_autosave == false) {
			notepad_window.data('changed', false);
			notepad_window.data('filename', filename);
			notepad_window.set_header(filename);
		}
	}, function() {
		if (is_autosave == false) {
			orb_alert('Error while saving file.', 'Error');
		}
	});
}

function notepad_set_cursor(notepad_window, position, select = 0) {
	var elem = notepad_window.find('textarea')[0];

	elem.focus();
	elem.setSelectionRange(position, position + select);
}

function notepad_tab_pressed(notepad_window, event) {
	var textarea = notepad_window.find('textarea');

	if (event.which != 9) {
		return;
	}

	event.preventDefault();
	var start = textarea[0].selectionStart;
	var end = textarea[0].selectionEnd;
	var text = textarea.val();
	var selText = text.substring(start, end);
	textarea.val(text.substring(0, start) + '\t' +
		selText.replace(/\n/g, '\n\t') +
		text.substring(end));
	textarea[0].selectionStart = textarea[0].selectionEnd = start + 1;

	notepad_window.data('changed', true);
}

function notepad_find(notepad_window) {
	var find_content =
		'<div class="notepad_find_replace">' +
		'<div class="form-group">' +
		'<input type="text" placeholder="Find" class="form-control find" />' +
		'</div>' +
		'<div class="btn-group">' +
		'<input type="button" value="Find" class="btn btn-default action" />' +
		'<input type="button" value="Cancel" class="btn btn-default cancel" />' +
		'</div>' +
		'</div>';

	var notepad_find_window = $(find_content).orb_window({
		header: 'Find',
		width: 500,
		height: 100,
		icon: NOTEPAD_ICON,
		dialog: true,
		close: function() {
			$(document).off('keydown', key_handler);
		}
	});

	notepad_find_window.find('input.action').click(function() {
		var find = notepad_find_window.find('input.find').val();

		if (find != '') {
			var text = notepad_window.find('textarea').val();
			if ((position = text.indexOf(find)) > -1) {
				notepad_window.data('find_str', find);
				notepad_window.data('find_pos', position);
				notepad_set_cursor(notepad_window, position, find.length);
			} else {
				orb_alert('Text not found.');
			}

			notepad_find_window.close();
		}
	});

	notepad_find_window.find('input.cancel').click(function() {
		notepad_find_window.close();
	});

	notepad_find_window.open();
	notepad_find_window.find('input.find').focus();

	var key_handler = function(event) {
		if (event.which == 27) {
			notepad_find_window.find('div.btn-group input.cancel').trigger('click');
		}
	};
	$(document).on('keydown', key_handler);
}

function notepad_find_next(notepad_window) {
	var find = notepad_window.data('find_str');
	var position = notepad_window.data('find_pos');

	if ((find != undefined) && (position != undefined)) {
		var text = notepad_window.find('textarea').val();
		if ((position = text.indexOf(find, position + 1)) > -1) {
			notepad_window.data('find_pos', position);
			notepad_set_cursor(notepad_window, position, find.length);
		} else {
			notepad_window.data('find_str', null);
			notepad_window.data('find_pos', null);
			orb_alert('No more appearances found.');
		}
	} else {
		notepad_find(notepad_window);
	}
}

function notepad_find_replace(notepad_window) {
	var replace_content =
		'<div class="notepad_find_replace">' +
		'<div class="form-group">' +
		'<input type="text" placeholder="Find" class="form-control find" />' +
		'</div>' +
		'<div class="form-group">' +
		'<input type="text" placeholder="Replace" class="form-control replace" />' +
		'</div>' +
		'<div class="btn-group">' +
		'<input type="button" value="Find & Replace" class="btn btn-default action" />' +
		'<input type="button" value="Cancel" class="btn btn-default cancel" />' +
		'</div>' +
		'</div>';

	var notepad_replace_window = $(replace_content).orb_window({
		header: 'Find & replace',
		width: 500,
		height: 135,
		icon: NOTEPAD_ICON,
		dialog: true,
		close: function() {
			$(document).off('keydown', key_handler);
		}
	});

	notepad_replace_window.find('input.action').click(function() {
		var find = notepad_replace_window.find('input.find').val();
		var replace = notepad_replace_window.find('input.replace').val();

		if (find.length > 0) {
			var text = notepad_window.find('textarea').val();

			var position = 0;
			while ((position = text.indexOf(find, position)) > -1) {
				text = text.substr(0, position) + replace + text.substr(position + find.length);
				position += replace.length;
				notepad_window.data('changed', true);
			}

			notepad_window.find('textarea').val(text);
		}

		notepad_replace_window.close();
	});

	notepad_replace_window.find('input.cancel').click(function() {
		notepad_replace_window.close();
	});

	notepad_replace_window.open();
	notepad_replace_window.find('input.find').focus();

	var key_handler = function(event) {
		if (event.which == 27) {
			notepad_replace_window.find('div.btn-group input.cancel').trigger('click');
		}
	};
	$(document).on('keydown', key_handler);
}

function notepad_wordwrap(notepad_window, value) {
	value = ((value === true) || (value === 'true'));

	notepad_window.find('textarea').attr('wrap', value ? '' : 'off');

	orb_setting_set('applications/notepad/wordwrap', value);
	
	var menu = notepad_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) li:nth-child(1)');
	if (value) {
		menu.addClass('selected');
	} else {
		menu.removeClass('selected');
	}
}

function notepad_menu_click(notepad_window, item) {
	switch (item) {
		case 'New':
			if (notepad_dont_discard(notepad_window)) {
				break;
			}

			notepad_window.find('textarea').val('');
			notepad_window.data('changed', false);
			notepad_window.data('filename', null);
			notepad_focus(notepad_window);
			notepad_window.set_header('');
			break;
		case 'Open':
			if (notepad_dont_discard(notepad_window)) {
				break;
			}

			orb_file_dialog('Open', function(filename) {
				if (notepad_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					orb_file_open(filename, function(content) {
						content = utf8_decode(content);
						notepad_window.find('textarea').val(content);
						notepad_window.data('changed', false);
						notepad_window.data('filename', filename);
						notepad_window.set_header(filename);
						notepad_focus(notepad_window, 0);
					}, function() {
						orb_alert('File not found.', 'Error');
					});
				}
			}, 'Documents');
			break;
		case 'Save':
			var filename = notepad_window.data('filename');
			if (filename != undefined) {
				notepad_save_file(notepad_window, filename);
				break;
			}
		case 'Save as':
			var filename = notepad_window.data('filename');
			if (filename == undefined) {
				var start_dir = 'Documents';
				var start_file = undefined;
			} else {
				var start_dir = orb_file_dirname(filename);
				var start_file = orb_file_filename(filename);
			}

			orb_file_dialog('Save', function(filename) {
				if (notepad_valid_extension(filename) == false) {
					filename += '.txt';
				} else if (orb_file_extension(filename) == ORB_NO_EXTENSION) {
					filename += '.txt';
				}

				if (filename != notepad_window.data('filename')) {
					orb_file_exists(filename, function(exists) {
						if (exists) {
							if (confirm('File already exists. Overwrite?') == false) {
								return;
							}
						}
						notepad_save_file(notepad_window, filename);
					}, function() {
						orb_alert('Error while saving file.', 'Error');
					});
				} else {
					notepad_save_file(notepad_window, filename);
				}
			}, start_dir, start_file);
			break;
		case 'Find':
			notepad_find(notepad_window);
			break;
		case 'Find next':
			notepad_find_next(notepad_window);
			break;
		case 'Find & Replace':
			notepad_find_replace(notepad_window);
			break;
		case 'Word wrap':
			var wordwrap = (notepad_window.find('textarea').attr('wrap') == 'off');
			notepad_wordwrap(notepad_window, wordwrap);
			break;
		case 'Information':
			var content = notepad_window.find('textarea').val();

			var characters = content.length;

			content = content.trim().replace(/\s+/g, ' ');
			var words = content.match(/ /g);

			if (words != null) {
				words = words.length + 1;
			} else if (content.length > 0) {
				words = 1;
			} else {
				words = 0;
			}

			orb_alert('Characters: ' + characters + '\nWords: ' + words, 'Information');
			break;
		case 'Exit':
			notepad_window.close();
			break;
		case 'About':
			orb_alert('<img src="' + NOTEPAD_ICON + '" class="about" draggable="false" />Notepad\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function notepad_open_icon(icon) {
	var filename = orb_icon_to_filename(icon);

	if (filename == undefined) {
		return;
	}

	notepad_open(filename);
}

function notepad_open(filename = undefined) {
	var window_content =
		'<div class="notepad">' +
		'<textarea style="resize:none" class="form-control"></textarea>' +
		'</div>';

	var notepad_window = $(window_content).orb_window({
		header: 'Notepad',
		width: 800,
		height: 500,
		icon: NOTEPAD_ICON,
		menu: {
			'File': [ 'New', 'Open', 'Save', 'Save as', '-', 'Exit' ],
			'Search': [ 'Find', 'Find next', 'Find & Replace' ],
			'View': [ 'Word wrap', 'Information' ],
			'Help': [ 'About' ]
		},
		menuCallback: notepad_menu_click,
		close: function() {
			if (notepad_dont_discard(notepad_window)) {
				return false;
			}

			clearInterval(autosave);
			orb_file_remove(autosave_filename);
		}
	});

	var autosave_base = 'Temporary/notepad_autosave_';
	if (filename != undefined) {
		var autosave_load = (filename.substr(0, autosave_base.length) == autosave_base);
	} else {
		var autosave_load = false;
	}

	if (autosave_load) {
		var autosave_filename = filename;
	} else {
		var autosave_filename = autosave_base + Date.now() + '.txt';
	}

	notepad_window.data('changed', autosave_load);
	notepad_window.data('filename', null);
	notepad_window.find('textarea').on('input', function() {
		notepad_window.data('changed', true);
	});

	orb_setting_get('applications/notepad/wordwrap', function(value) {
		notepad_wordwrap(notepad_window, value);
	});

	notepad_window.find('textarea').keydown(function(event) {
		notepad_tab_pressed(notepad_window, event);
	});

	if (filename != undefined) {
		if (notepad_valid_extension(filename) == false) {
			notepad_window.close();
			orb_alert('Invalid file type.');
		} else {
			orb_file_open(filename, function(content) {
				content = utf8_decode(content);
				notepad_window.find('textarea').val(content);
				if (autosave_load == false) {
					notepad_window.data('filename', filename);
					notepad_window.set_header(filename);
				}
				notepad_window.open();
				notepad_focus(notepad_window, 0);
			}, function(code, text) {
				notepad_window.close();
				orb_alert('File not found.');
			});
		}
	} else {
		notepad_window.open();
		notepad_focus(notepad_window);
	}

	var autosave = setInterval(function() {
		if (notepad_window.data('changed') == false) {
			return;
		}

		notepad_save_file(notepad_window, autosave_filename, true);
	}, 5000);
}

$(document).ready(function() {
	orb_startmenu_add('Notepad', NOTEPAD_ICON, notepad_open);
	orb_upon_file_open('txt', notepad_open, NOTEPAD_ICON);
	orb_upon_file_open('log', notepad_open, NOTEPAD_ICON);
	orb_upon_file_open(ORB_NO_EXTENSION, notepad_open, NOTEPAD_ICON);

	orb_contextmenu_extra_item('ots', 'Edit with Notepad', 'i-cursor', notepad_open_icon);
});
