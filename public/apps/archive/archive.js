/* Orb Archive application
 *
 * Let every function in this file start with the application name
 * to avoid conflicts with other applications,
 *
 * Always use archive_window to interact with your application to
 * avoid issues with multiple instances of your application.
 */

const ARCHIVE_ICON = '/apps/archive/archive.png';

var archive_extensions = [];

function archive_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	return archive_extensions.includes(extension.toLowerCase());
}

function archive_list(container, list, directory = null) {
	if (directory == null) {
		directory = '';
	}

	var new_container = $('<div class="directory"><span class="name">' + list.attr('name') + '</span></div>');
	container.append(new_container);
	container = new_container;

	list.children('directory').each(function() {
		archive_list(container, $(this), directory + $(this).attr('name') + '/');
	});

	list.children('file').each(function() {
		var extension = orb_file_extension($(this).text());
		var icon = orb_get_file_icon(extension);
		var size = orb_file_nice_size($(this).attr('size'));
		var file = '<div class="file" path="' + directory + '">' +
			'<img src="' + icon + '" class="icon" draggable="false" />' +
			'<span class="name">' + $(this).text() + '</span>' +
			'<span class="size">' + size + '</span>' +
			'</div>';
		container.append(file);
	});
}

function archive_extract(archive_window, filename, callback = undefined) {
	var archive = archive_window.data('archive');

	$.post('/archive/extract', {
		archive: archive,
		filename: filename,
		directory: 'Temporary'
	}).done(function(data) {
		var filename = $(data).find('filename').text();

		orb_directory_notify_update('Temporary');

		var path = orb_file_dirname(filename);
		if (path != 'Temporary') {
			orb_directory_notify_update(path);
		}

		if (callback != undefined) {
			callback(filename);
		}
	}).fail(function(data) {
		orb_alert('Error extracting file.');
	});
}

function archive_contextmenu_handler(target, option) {
	var archive_window = target;
	while (archive_window.hasClass('archive') == false) {
		archive_window = archive_window.parent();
	}
	var filename = target.find('span.name').text();
	var path = target.attr('path');

    switch (option) {
        case 'Extract to Desktop':
			archive_extract(archive_window, path + filename, function(filename) {
				orb_file_move(filename, 'Desktop', function() {
					orb_desktop_refresh();
				});
			});
			break;
        case 'Download':
			archive_extract(archive_window, path + filename, function(filename) {
				window.open(orb_download_url(filename), '_blank');
			});
			break;
	}
}

function archive_open_archive(archive_window, archive) {
	$.ajax({
		url: '/archive/list/' + url_encode(archive)
	}).done(function(data) {
		archive_window.data('archive', archive);

		archive_window.empty();
		archive_list(archive_window, $(data).find('output').children('directory'));

		archive_window.children('div').show();
		archive_window.children('div').children('div').show();

		archive_window.find('div.directory').each(function() {
			if ($(this).children('div').length > 0) {
				var type = $(this).children('div').first().is(':visible') ? 'down' : 'right';
				$(this).prepend('<span class="fa fa-chevron-' + type + '"></span>');
			}
		});

		archive_window.find('div.file').on('click', function() {
			archive_window.find('div.selected').removeClass('selected');
			$(this).addClass('selected');
		});

		archive_window.find('div.file').on('contextmenu', function(event) {
			var menu_entries = [
				{ name: 'Extract to Desktop', icon: 'desktop' },
				{ name: 'Download', icon: 'download' }
			];

			orb_contextmenu_show($(this), event, menu_entries, archive_contextmenu_handler);

			event.stopPropagation();
			return false;
		});

		archive_window.find('div.file span').on('click', function() {
			$(this).parent().trigger('click');
		});

		archive_window.find('div.file').on('dblclick', function() {
			var filename = $(this).find('span.name').text();
			var path = $(this).attr('path');
			archive_extract(archive_window, path + filename, function(filename) {
				var extension = orb_file_extension(filename);
				var handler = orb_get_file_handler(extension);
				if (handler != undefined) {
					handler(filename);
				} else {
					window.open(orb_download_url(filename), '_blank');
				}
			});
		});

		archive_window.find('div.directory span').on('click', function(event) {
			var dir = $(this).parent();
			dir.children('div').toggle();

			var type = dir.children('div').first().is(':visible') ? 'down' : 'right';
			dir.children('span.fa').removeClass('fa-chevron-down');
			dir.children('span.fa').removeClass('fa-chevron-right');
			dir.children('span.fa').addClass('fa-chevron-' + type);
			return false;
		});
	}).fail(function() {
		orb_alert('Error opening archive.');
	});
}

function archive_menu_click(archive_window, item) {
	switch (item) {
		case 'Open':
			orb_file_dialog('Open', function(filename) {
				if (archive_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					archive_open_archive(archive_window, filename);
				}
			});
			break;
		case 'Exit':
			archive_window.close();
			break;
		case 'About':
			var extensions = archive_extensions.join(', ');
			orb_alert('<img src="' + ARCHIVE_ICON + '" class="about" draggable="false" />Archive browser\nCopyright (c) by Hugo Leisink\nSupported file extensions: ' + extensions, 'About');
			break;
	}
}

function archive_open(filename = undefined) {
	var window_content =
		'<div class="archive">' +
		'</div>';

	var archive_window = $(window_content).orb_window({
		header:'Archive',
		icon: ARCHIVE_ICON,
		width: 760,
		height: 430,
		menu: {
			'File': [ 'Open', 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: archive_menu_click
	});

	archive_window.open();

	if (filename != undefined) {
		archive_open_archive(archive_window, filename);
	}
}

$(document).ready(function() {
	orb_startmenu_add('Archive', ARCHIVE_ICON, archive_open);

	$.ajax({
		url: '/archive/extensions'
	}).done(function(data) {
		$(data).find('extension').each(function() {
			var extension = $(this).text();
			archive_extensions.push(extension);
			orb_upon_file_open(extension, archive_open, ARCHIVE_ICON);
		});

	});
});
