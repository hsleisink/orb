/* Orb Wolf3D application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const WOLF3D_ICON = '/apps/wolf3d/wolf3d.png';

function wolf3d_show_map(episode, level) {
	var map = '/apps/wolf3d/maps/E' + episode + 'L' + level.toString().padStart(2, '0') + '.png';
	var header = 'Wolfenstein 3D - Map episode ' + episode + ' level ' + level;

	var current = $('div.wolf3d_map');
	if (current.length > 0) {
		current.find('img').attr('src', map);
		current.parent().parent().find('div.window-header div.title').text(header);
		return;
	}

	var window_content =
		'<div class="wolf3d_map"><img src="' + map + '" draggable="false" /></div>';

	var wolf3d_map_window = $(window_content).orb_window({
		header: header,
		icon: WOLF3D_ICON,
		width: 700,
		height: 680,
		maximize: false,
		resize: false,
	});

	wolf3d_map_window.open();
}

function wolf3d_menu_click(wolf3d_window, item) {
	switch (item) {
		case 'Fullscreen':
			wolf3d_window.find('iframe')[0].contentWindow.fs();
			break;
		case 'Exit':
			wolf3d_window.close();
			break;
		case 'About':
			orb_alert('<img src="' + WOLF3D_ICON + '" class="about" draggable="false" />Wolfenstein 3D\nCopyright (c) by ID Software', 'About');
			break;
		default:
			var parts = item.split(', ');
			var episode = parts[0].split(' ')[1];
			var level = parts[1].split(' ')[1];
			wolf3d_show_map(episode, level);
			break;
	}
}

function wolf3d_open(filename = undefined) {
	var window_content =
		'<div class="wolf3d"><iframe id="wolf3d" src="/apps/wolf3d/wolf3d.html"></iframe></div>';

	var maps = [];
	for (e = 1; e <= 3; e++) {
		maps[e] = [];
		for (l = 1; l <= 10; l++) {	
			maps[e][l] = 'Episode ' + e + ', level ' + l;
		}
	}

	var wolf3d_window = $(window_content).orb_window({
		header:'Wolfenstein 3D',
		icon: WOLF3D_ICON,
		width: 660,
		height: 432,
		maximize: false,
		resize: false,
		menu: {
			'Game': [ 'Fullscreen', 'Exit' ],
			'Maps episode 1': maps[1],
			'Maps episode 2': maps[2],
			'Maps episode 3': maps[3],
			'Help': [ 'About' ]
		},
		menuCallback: wolf3d_menu_click,
		close: function() {
			var map = $('div.wolf3d_map');
			if (map.length > 0) {
				map.close();
			}
		}
	});

	wolf3d_window.open();
}

$(document).ready(function() {
	orb_startmenu_add('Wolfenstein 3D', WOLF3D_ICON, wolf3d_open);
});
