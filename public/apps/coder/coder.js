/* Orb Coder application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const CODER_ICON = '/apps/coder/coder.png';

const CODER_DEFAULT_THEME = 'twilight';
const CODER_EXTENSIONS = [ 'c', 'class', 'css', 'h', 'html', 'lua', 'js', 'json', 'php', 'py', 'rs', 'sql', 'xml', 'xslt' ];

function coder_focus(coder_window) {
	var editor = coder_window.data('editor');

	editor.focus();
}

function coder_dont_discard(coder_window) {
	if (coder_window.data('changed')) {
		if (confirm('File has been changed. Discard?') == false) {
			return true;
		}
	}

	return false;
}

function coder_valid_extension(filename) {
	var extension = orb_file_extension(filename);

	if (extension == false) {
		return false;
	}

	var extensions = CODER_EXTENSIONS;
	extensions.push('txt', ORB_NO_EXTENSION);
	extensions.push('ujs', ORB_NO_EXTENSION);

	return extensions.includes(extension.toLowerCase());
}

function coder_save_file(coder_window, filename, is_autosave = false) {
	var editor = coder_window.data('editor');
	var content = editor.getValue();

	orb_file_save(filename, content, false, function() {
		if (is_autosave == false) {
			coder_window.data('changed', false);
			coder_window.data('filename', filename);
			coder_window.data(filename);
		}
	}, function() {
		if (is_autosave == false) {
			orb_alert('Error while saving file.', 'Error');
		}
	});
}

function coder_set_cursor(coder_window, position, select = 0) {
	var elem = coder_window.find('textarea')[0];

	elem.focus();
	elem.setSelectionRange(position, position + select);
}

function coder_find(coder_window) {
	var find_content =
		'<div class="coder_find_replace">' +
		'<div class="form-group">' +
		'<input type="text" placeholder="Find" class="form-control find" />' +
		'</div>' +
		'<div class="btn-group">' +
		'<input type="button" value="Find" class="btn btn-default action" />' +
		'<input type="button" value="Cancel" class="btn btn-default cancel" />' +
		'</div>' +
		'</div>';

	var coder_find_window = $(find_content).orb_window({
		header: 'Find',
		width: 500,
		height: 100,
		icon: CODER_ICON,
		dialog: true,
		close: function() {
			$(document).off('keydown', key_handler);
		}
	});

	coder_find_window.find('input.action').on('click', function() {
		var find = coder_find_window.find('input.find').val();

		if (find != '') {
			var editor = coder_window.data('editor');
			editor.find(find);
		}

		coder_find_window.close();
	});

	coder_find_window.find('input.cancel').on('click', function() {
		coder_find_window.close();
	});

	coder_find_window.open();
	coder_find_window.find('input.find').focus();

	var key_handler = function(event) {
	if (event.which == 27) {
			coder_find_window.find('div.btn-group input.cancel').trigger('click');
		}
	};
	$(document).on('keydown', key_handler);
}

function coder_find_next(coder_window) {
	var editor = coder_window.data('editor');
	editor.findNext();
}

function coder_find_replace(coder_window) {
	var replace_content =
		'<div class="coder_find_replace">' +
		'<div class="form-group">' +
		'<input type="text" placeholder="Find" class="form-control find" />' +
		'</div>' +
		'<div class="form-group">' +
		'<input type="text" placeholder="Replace" class="form-control replace" />' +
		'</div>' +
		'<div class="btn-group">' +
		'<input type="button" value="Find & Replace" class="btn btn-default action" />' +
		'<input type="button" value="Cancel" class="btn btn-default cancel" />' +
		'</div>' +
		'</div>';

	var coder_replace_window = $(replace_content).orb_window({
		header: 'Find & replace',
		width: 500,
		height: 135,
		icon: CODER_ICON,
		dialog: true,
		close: function() {
			$(document).off('keydown', key_handler);
		}
	});

	coder_replace_window.find('input.action').on('click', function() {
		var find = coder_replace_window.find('input.find').val();
		var replace = coder_replace_window.find('input.replace').val();

		if (find.length > 0) {
			var editor = coder_window.data('editor');
			editor.replaceAll(replace, { needle: find });
		}

		coder_replace_window.close();
	});

	coder_replace_window.find('input.cancel').on('click', function() {
		coder_replace_window.close();
	});

	coder_replace_window.open();
	coder_replace_window.find('input.find').focus();

	var key_handler = function(event) {
	if (event.which == 27) {
			coder_replace_window.find('div.btn-group input.cancel').trigger('click');
		}
	};
	$(document).on('keydown', key_handler);
}

function coder_set_mode(coder_window, extension) {
	var editor = coder_window.data('editor');

	coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li').removeClass('selected');

	switch (extension) {
		case ORB_NO_EXTENSION:
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(1)').addClass('selected');
			editor.setOption('mode', 'ace/mode/text');
			break;
		case 'c':
		case 'h':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(2)').addClass('selected');
			editor.setOption('mode', 'ace/mode/css');
			break;
		case 'css':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(3)').addClass('selected');
			editor.setOption('mode', 'ace/mode/css');
			break;
		case 'html':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(4)').addClass('selected');
			editor.setOption('mode', 'ace/mode/html');
			break;
		case 'class':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(5)').addClass('selected');
			editor.setOption('mode', 'ace/mode/java');
			break;
		case 'js':
		case 'ujs':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(6)').addClass('selected');
			editor.setOption('mode', 'ace/mode/javascript');
			break;
		case 'json':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(7)').addClass('selected');
			editor.setOption('mode', 'ace/mode/json');
			break;
		case 'lua':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(8)').addClass('selected');
			editor.setOption('mode', 'ace/mode/lua');
			break;
		case 'php':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(9)').addClass('selected');
			editor.setOption('mode', 'ace/mode/php');
			break;
		case 'py':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(10)').addClass('selected');
			editor.setOption('mode', 'ace/mode/php');
			break;
		case 'rs':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(11)').addClass('selected');
			editor.setOption('mode', 'ace/mode/rust');
			break;
		case 'sql':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(12)').addClass('selected');
			editor.setOption('mode', 'ace/mode/sql');
			break;
		case 'xml':
		case 'xslt':
			coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(3) ul li:nth-child(13)').addClass('selected');
			editor.setOption('mode', 'ace/mode/xml');
	}
}

function coder_set_theme(coder_window, theme) {
	var editor = coder_window.data('editor');

	orb_setting_set('applications/coder/theme', theme);

	var themes = [ 'ambiance', 'chaos', 'chrome', 'cobalt', 'dawn', 'github', 'kuroir', 'merbivore', 'monokai', 'terminal', 'twilight', 'xcode' ];
	menu_item = themes.indexOf(theme) + 1;

	theme = 'ace/theme/' + theme;
	editor.setTheme(theme);

	coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(4) ul li').removeClass('selected');
	coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(4) ul li:nth-child(' + menu_item + ')').addClass('selected');
}

function coder_update_menu(coder_window) {
	var editor = coder_window.data('editor');
	var wordwrap = coder_window.parent().parent().find('ul.nav li.dropdown:nth-child(2) ul li:nth-child(5)');

	if (editor.getOption('wrap') != 'off') {
		wordwrap.addClass('selected');
	} else {
		wordwrap.removeClass('selected');
	}
}

function coder_menu_click(coder_window, item) {
	var editor = coder_window.data('editor');

	switch (item) {
		case 'New':
			if (coder_dont_discard(coder_window)) {
				break;
			}

			editor.session.setValue('');
			coder_window.data('changed', false);
			coder_window.data('filename', null);
			coder_window.set_header('');
			coder_focus(coder_window);
			break;
		case 'Open':
			if (coder_dont_discard(coder_window)) {
				break;
			}

			orb_file_dialog('Open', function(filename) {
				if (coder_valid_extension(filename) == false) {
					orb_alert('Invalid file type.');
				} else {
					orb_file_open(filename, function(content) {
						var extension = orb_file_extension(filename);
						coder_set_mode(coder_window, extension);

						editor.session.setValue(content, -1);

						coder_window.data('changed', false);
						coder_window.data('filename', filename);
						coder_window.set_header(filename);
						coder_focus(coder_window);
					}, function() {
						orb_alert('File not found.', 'Error');
					});
				}
			});
			break;
		case 'Save':
			var filename = coder_window.data('filename');
			if (filename != undefined) {
				coder_save_file(coder_window, filename);
				break;
			}
		case 'Save as':
			var filename = coder_window.data('filename');
			if (filename == undefined) {
				var start_dir = 'Documents';
				var start_file = undefined;
			} else {
				var start_dir = orb_file_dirname(filename);
				var start_file = orb_file_filename(filename);
			}

			orb_file_dialog('Save', function(filename) {
				if (coder_valid_extension(filename) == false) {
					filename += '.txt';
				}

				if (filename != coder_window.data('filename')) {
					orb_file_exists(filename, function(exists) {
						if (exists) {
							if (confirm('File already exists. Overwrite?') == false) {
								return;
							}
						}
						coder_save_file(coder_window, filename);
					}, function() {
						orb_alert('Error while saving file.', 'Error');
					});
				} else {
					coder_save_file(coder_window, filename);
				}
			}, start_dir, start_file);
			break;
		case 'Exit':
			coder_window.close();
			break;
		case 'Find':
			coder_find(coder_window);
			break;
		case 'Find next':
			coder_find_next(coder_window);
			break;
		case 'Find & Replace':
			coder_find_replace(coder_window);
			break;
		case 'Word wrap':
			var wrap = (editor.getOption('wrap') == 'off');
			editor.setOption('wrap', wrap);
			orb_setting_set('applications/coder/wordwrap', wrap);
			coder_update_menu(coder_window);
			break;
		case 'Default':
			coder_set_mode(coder_window, ORB_NO_EXTENSION);
			break;
		case 'C / C++':
			coder_set_mode(coder_window, 'c');
			break;
		case 'CSS':
			coder_set_mode(coder_window, 'css');
			break;
		case 'HTML':
			coder_set_mode(coder_window, 'html');
			break;
		case 'Java':
			coder_set_mode(coder_window, 'java');
			break;
		case 'Javascript':
			coder_set_mode(coder_window, 'js');
			break;
		case 'JSON':
			coder_set_mode(coder_window, 'json');
			break;
		case 'PHP':
			coder_set_mode(coder_window, 'php');
			break;
		case 'Python':
			coder_set_mode(coder_window, 'py');
			break;
		case 'Rust':
			coder_set_mode(coder_window, 'rs');
			break;
		case 'SQL':
			coder_set_mode(coder_window, 'sql');
			break;
		case 'XML':
			coder_set_mode(coder_window, 'xml');
			break;
		case 'Ambiance':
			coder_set_theme(coder_window, 'ambiance');
			break;
		case 'Chaos':
			coder_set_theme(coder_window, 'chaos');
			break;
		case 'Chrome':
			coder_set_theme(coder_window, 'chrome');
			break;
		case 'Cobalt':
			coder_set_theme(coder_window, 'cobalt');
			break;
		case 'Dawn':
			coder_set_theme(coder_window, 'dawn');
			break;
		case 'GitHub':
			coder_set_theme(coder_window, 'github');
			break;
		case 'Kuroir':
			coder_set_theme(coder_window, 'kuroir');
			break;
		case 'Merbivore':
			coder_set_theme(coder_window, 'merbivore');
			break;
		case 'Monokai':
			coder_set_theme(coder_window, 'monokai');
			break;
		case 'Terminal':
			coder_set_theme(coder_window, 'terminal');
			break;
		case 'Twilight':
			coder_set_theme(coder_window, 'twilight');
			break;
		case 'XCode':
			coder_set_theme(coder_window, 'xcode');
			break;
		case 'About':
			var message = [
				'<img src="' + CODER_ICON + '" class="about" draggable="false" />Coder',
				'Copyright (c) by Hugo Leisink', '',
				'Ace editor',
				'Copyright (c) by Ajax.org',
				'<a href="https://ace.c9.io/" target="_blank">https://ace.c9.io/</a>'
			];
			orb_alert(message.join('\n'), 'About');

			break;
	}
}

function coder_open_icon(icon) {
	var filename = orb_icon_to_filename(icon);

	if (filename == undefined) {
		return;
	}

	coder_open(filename);
}

function coder_open(filename = undefined) {
	var window_content =
		'<div class="coder">' +
		'<div class="editor"></div>' +
		'</div>';

	var coder_window = $(window_content).orb_window({
		header: 'Coder',
		width: 800,
		height: 500,
		icon: CODER_ICON,
		menu: {
			'File': [ 'New', 'Open', 'Save', 'Save as', '-', 'Exit' ],
			'Edit': [ 'Find', 'Find next', 'Find & Replace', '-', 'Word wrap' ],
			'Mode': [ 'Default', 'C / C++', 'CSS', 'HTML', 'Java', 'Javascript', 'JSON', 'Lua', 'PHP', 'Python', 'Rust', 'SQL', 'XML' ],
			'Theme': [ 'Ambiance', 'Chaos', 'Chrome', 'Cobalt', 'Dawn', 'GitHub', 'Kuroir', 'Merbivore', 'Monokai', 'Terminal', 'Twilight', 'XCode' ],
			'Help': [ 'About' ]
		},
		menuCallback: coder_menu_click,
		close: function() {
			if (coder_dont_discard(coder_window)) {
				return false;
			}

			clearInterval(autosave);
			orb_file_remove(autosave_filename);
		}
	});

	var autosave_base = 'Temporary/coder_autosave_';
	if (filename != undefined) {
		var autosave_load = (filename.substr(0, autosave_base.length) == autosave_base);
	} else {
		var autosave_load = false;
	}

	if (autosave_load) {
		var autosave_filename = filename;
	} else {
		var autosave_filename = autosave_base + Date.now() + '.txt';
	}

	coder_window.data('changed', autosave_load);
	coder_window.data('filename', null);

	orb_load_javascript('/apps/coder/ace-builds/src-min-noconflict/ace.js');

	var editor = ace.edit(coder_window.find('div.editor')[0]);

	editor.session.setUseSoftTabs(false);

	orb_setting_get('applications/coder/theme', function(value) {
		coder_set_theme(coder_window, value);
	}, function() {
		coder_set_theme(coder_window, CODER_DEFAULT_THEME);
	});

	orb_setting_get('applications/coder/wordwrap', function(value) {
		if (value == 'true') {
			editor.setOption('wrap', true);
		}
		coder_update_menu(coder_window);
	});

	editor.on('change', function() {
		coder_window.data('changed', true);
	});

	coder_window.parent().parent().find('div.ui-resizable-handle').on('mouseup', function() {
		editor.resize();
	});

	coder_window.data('editor', editor);

	coder_focus(coder_window);
	coder_set_mode(coder_window, ORB_NO_EXTENSION);

	if (filename != undefined) {
		if (coder_valid_extension(filename) == false) {
			coder_window.close();
			orb_alert('Invalid file type.');
		} else {
			orb_file_open(filename, function(content) {
				var extension = orb_file_extension(filename);
				coder_set_mode(coder_window, extension);
				editor.session.setValue(content, -1);
				coder_window.data('changed', false);
				if (autosave_load == false) {
					coder_window.data('filename', filename);
					coder_window.set_header(filename);
				}
				coder_window.open();
			}, function() {
				coder_window.close();
				orb_alert('File not found.');
			});
		}
	} else {
		coder_window.open();
	}

	coder_window.on('click', function() {
		coder_focus(coder_window);
	});

	var autosave = setInterval(function() {
		if (coder_window.data('changed') == false) {
			return;
		}

		coder_save_file(coder_window, autosave_filename, true);
	}, 5000);
}

$(document).ready(function() {
	orb_startmenu_add('Coder', CODER_ICON, coder_open);

	CODER_EXTENSIONS.forEach(function(extension) {
		orb_upon_file_open(extension, coder_open, CODER_ICON);
	});

	orb_contextmenu_extra_item('ujs', 'Edit with Coder', 'i-cursor', coder_open_icon);
	orb_contextmenu_extra_item(ORB_NO_EXTENSION, 'Edit with Coder', 'i-cursor', coder_open_icon);
});
