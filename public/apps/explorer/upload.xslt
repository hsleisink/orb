<?xml version="1.0" ?>
<!--
//
//	Copyright (c) by Hugo Leisink <hugo@leisink.net>
//	This file is part of the Orb web desktop
//	https://gitlab.com/hsleisink/orb
//
//	Licensed under the GPLv2 License
//
-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-system="about:legacy-compat" />

<!--
//
//  Upload template
//
-->
<xsl:template match="upload">
<div class="content">
<h1>Upload files</h1>
<form action="/apps/explorer/upload.php?user={username}&amp;upload={key}" class="dropzone" id="dropzone">
<div class="dz-message"><span>Drop files here to upload them.</span></div>
<input type="hidden" name="path" value="" />
</form>
</div>
<div class="generated">Uploading via <a href="https://gitlab.com/hsleisink/orb" target="_blank">Orb</a></div>
</xsl:template>

<!--
//
//  Error template
//
-->
<xsl:template match="error">
<div class="content notification">
<h1>Orb upload error</h1>
<p><xsl:value-of select="." /></p>
</div>
</xsl:template>

<!--
//
//  Output template
//
-->
<xsl:template match="/output">
<html lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="generator" content="File" />
<link rel="apple-touch-icon" href="/images/orb.png" />
<link rel="icon" href="/images/orb.png" />
<link rel="shortcut icon" href="/images/orb.png" />
<title>Orb file upload</title>
<xsl:for-each select="styles/style">
<link rel="stylesheet" type="text/css" href="{.}" />
</xsl:for-each>
<xsl:for-each select="javascripts/javascript">
<script type="text/javascript" src="{.}" /><xsl:text>
</xsl:text></xsl:for-each>
</head>

<body>
<xsl:apply-templates select="upload" />
<xsl:apply-templates select="error" />
</body>

</html>
</xsl:template>

</xsl:stylesheet>
