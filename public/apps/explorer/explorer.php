<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Orb web desktop
	 * https://gitlab.com/hsleisink/orb
	 *
	 * Licensed under the GPLv2 License
	 */

	namespace Orb;

	if (defined("ORB_VERSION") == false) exit;

	class explorer extends orb_backend {
		private $shares = null;

		/* Upload file
		 */
		private function directory_size($directory) {
			if (($dp = opendir($directory)) == false) {
				return false;
			}

			$result = 0;

			while (($file = readdir($dp)) != false) {
				if (($file == ".") || ($file == "..")) {
					continue;
				}

				$path = $directory."/".$file;
				if (is_dir($path)) {
					$result += $this->directory_size($path);
				} else if (($size = filesize($path)) !== false) {
					$result += $size;
				}
			}

			closedir($dp);

			return $result;
		}

		/* Download URL
		 */
		public function post_download() {
			$url = $_POST["url"];
			$path = $_POST["path"];

			$parts = explode("/", $url);
			$filename = array_pop($parts);
			list($filename) = explode("?", $filename, 2);

			if ($filename == "") {
				$filename = array_pop($parts);
			}

			$target = $this->home_directory."/".$path."/".$filename;

			if (file_exists($target)) {
				$this->view->return_error(409);
				return;
			}

			if (($ch = curl_init($url)) == false) {
				$this->view->return_error(400);
				return;
			}

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			if (($st = curl_exec($ch)) === false) {
				curl_close($ch);
				$this->view->return_error(500);
				return;
			}

			if (($fd = fopen($target, "w")) == false) {
				curl_close($ch);
				$this->view->return_error(500);
				return;
			}

			fwrite($fd, $st);
			fclose($fd);

			curl_close($ch);
		}

		/* Upload file via drag & drop
		 */
		public function post() {
			if (is_true(READ_ONLY)) {
				$this->view->return_error(403);
				return;
			}

			if (is_array($_FILES["file"] ?? null) == false) {
				$this->view->return_error(400);
				return;
			}

			if ($_FILES["file"]["error"] != 0) {
				$this->view->return_error(400);
				return;
			}

			$path = trim($_POST["path"] ?? "", "/ ");
			if ($this->valid_filename($path) == false) {
				$this->view->return_error(400);
				return;
			}

			/* Check capacity
			 */
			$users = file(PASSWORD_FILE);

			$capacity = null;
			foreach ($users as $user) {
				$parts = explode(":", trim($user));

				if ($parts[0] != $this->username) {
					continue;
				} else if (count($parts) < 3) {
					continue;
				} else if ($parts[2] == "") {
					continue;
				}

				$capacity = (int)$parts[2];
			}

			if ($capacity !== null) {
				$total_size = $this->directory_size($this->home_directory);

				if ($total_size + (int)$_FILES["file"]["size"] > $capacity * MB) {
					$this->view->return_error(403);
					return;
				}
			}

			/* Move file
			 */
			$destination = $this->home_directory."/".$path;

			if (is_dir($destination) == false) {
				$this->view->return_error(404);
				return;
			}

			if ($path != "" ) {
				$destination .= "/";
			}
			$destination .= $_FILES["file"]["name"];

			move_uploaded_file($_FILES["file"]["tmp_name"], $destination);

			$this->view->add_tag("path", $path);
		}

		/* File share functions
		 */
		private function shares_load() {
			$shares_file = $this->home_directory."/.file_shares";

			if (file_exists($shares_file) == false) {
				$shares = array();
			} else {
				ob_start();
				$shares = file_get_contents($shares_file);
				ob_end_clean();
			
				$shares = json_decode($shares, true);
			}

			return $shares;
		}

		private function shares_save($shares) {
			if (($fp = fopen($this->home_directory."/.file_shares", "w")) == false) {
				return false;
			}

			fputs($fp, json_encode($shares));

			fclose($fp);

			return true;
		}

		public function post_share_create() {
			if ($this->valid_filename($_POST["filename"]) == false) {
				$this->view->return_error(406);
				return;
			}

			if (email::valid_address($_POST["email"] ?? null) == false) {
				$this->view->return_error(406);
				return;
			}

			$expire = (int)($_POST["expire"] ?? 1);
			if ($expire < 1) {
				$expire = 1;
			} else if ($expire > 365) {
				$expire = 365;
			}

			$password = strlen($_POST["password"] > 0) ? password_hash($_POST["password"], PASSWORD_ARGON2I) : "";

			$expire = date("Y-m-d", strtotime("+".$expire." days"));

			/* Load shares
			 */
			if (($shares = $this->shares_load()) === false) {
				$this->view->return_error(500);
				return;
			}

			/* New share
			 */
			do {
				$key = random_string(20);
			} while (isset($shares[$key]));

			$shares[$key] = implode("|", array($_POST["filename"], $password, $expire, $_POST["email"]));

			/* Save shares
			 */
			if ($this->shares_save($shares) == false) {
				$this->view->return_error(500);
				return;
			}

			/* Mail share
			 */
			$link_base = "https://".$_SERVER["HTTP_HOST"]."/apps/explorer/share.php";
			$link_params = "user=".$this->username."&share=".$key;

			$username = strtoupper(substr($this->username, 0, 1)).substr($this->username, 1);
			$expire = date("j F Y", strtotime($expire));

			$pwd_mesg = ($password != "") ? " The required password will be sent to you in a different way." : "";

			$message = file_get_contents(__DIR__."/share.txt");
			$fields = array(
				"FILE"     => basename($_POST["filename"]),
				"COMMENT"  => $_POST["comment"],
				"EXPIRE"   => $expire,
				"LINK"     => $link_base."?".$link_params,
				"USERNAME" => $username,
				"PASSWORD" => $pwd_mesg);

			$email = new orb_email("Orb file share", "no-reply@".$_SERVER["HTTP_HOST"]);
			$email->set_message_fields($fields);
			$email->message($message);
			$email->send($_POST["email"]);
		}

		public function get_share_list() {
			/* Load shares
			 */
			if (($shares = $this->shares_load()) === false) {
				$this->view->return_error(500);
				return;
			}

			$changed = false;

			foreach ($shares as $key => $share) {
				list($file, $password, $expire, $email) = explode("|", $share);

				$timestamp = strtotime($expire);
				if (time() >= $timestamp) {
					unset($shares[$key]);
					$changed = true;
					continue;
				}

				$expire = date("j F Y", $timestamp);

				$this->view->open_tag("share", array("key" => $key));
				$this->view->add_tag("file", $file);
				$this->view->add_tag("expire", $expire);
				$this->view->add_tag("password", show_boolean($password != ""));
				$this->view->add_tag("email", $email);
				$this->view->close_tag();
			}

			if ($changed == false) {
				return;
			}

			$this->shares_save($shares);
		}

		public function post_share_remove() {
			/* Load shares
			 */
			if (($shares = $this->shares_load()) === false) {
				$this->view->return_error(500);
				return;
			}

			if (isset($shares[$_POST["key"]]) == false) {
				$this->view->return_error(404);
				return;
			}

			unset($shares[$_POST["key"]]);

			/* Save shares
			 */
			if ($this->shares_save($shares) == false) {
				$this->view->return_error(500);
				return;
			}
		}

		/* Upload functions
		 */
		private function uploads_load() {
			$uploads_file = $this->home_directory."/.upload_directories";

			if (file_exists($uploads_file) == false) {
				$uploads = array();
			} else {
				ob_start();
				$uploads = file_get_contents($uploads_file);
				ob_end_clean();
			
				$uploads = json_decode($uploads, true);
			}

			return $uploads;
		}

		private function uploads_save($uploads) {
			if (($fp = fopen($this->home_directory."/.upload_directories", "w")) == false) {
				return false;
			}

			fputs($fp, json_encode($uploads));

			fclose($fp);

			return true;
		}

		public function post_upload_create() {
			if ($this->valid_filename($_POST["directory"]) == false) {
				$this->view->return_error(406);
				return;
			}

			if (email::valid_address($_POST["email"] ?? null) == false) {
				$this->view->return_error(406);
				return;
			}

			if (valid_input($_POST["max_files"], VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
				$this->view->return_error(406);
				return;
			}

			if (valid_input($_POST["max_size"], VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
				$this->view->return_error(406);
				return;
			}

			if (($_POST["max_files"] <= 0) || ($_POST["max_size"] <= 0)) {
				$this->view->return_error(406);
				return;
			}

			$expire = (int)($_POST["expire"] ?? 1);
			if ($expire < 1) {
				$expire = 1;
			} else if ($expire > 365) {
				$expire = 365;
			}

			$expire = date("Y-m-d", strtotime("+".$expire." days"));

			/* Load upload directories
			 */
			if (($uploads = $this->uploads_load()) === false) {
				$this->view->return_error(500);
				return;
			}

			/* New upload directory
			 */
			do {
				$key = random_string(20);
			} while (isset($uploads[$key]));

			$uploads[$key] = implode("|", array($_POST["directory"], $expire, $_POST["email"], $_POST["max_files"], $_POST["max_size"]));

			/* Save uploads
			 */
			if ($this->uploads_save($uploads) == false) {
				$this->view->return_error(500);
				return;
			}

			/* Mail upload directory
			 */
			$link_base = "https://".$_SERVER["HTTP_HOST"]."/apps/explorer/upload.php";
			$link_params = "user=".$this->username."&upload=".$key;

			$username = strtoupper(substr($this->username, 0, 1)).substr($this->username, 1);
			$expire = date("j F Y", strtotime($expire));

			$message = file_get_contents(__DIR__."/upload.txt");
			$fields = array(
				"COMMENT"  => $_POST["comment"],
				"EXPIRE"   => $expire,
				"LINK"     => $link_base."?".$link_params,
				"USERNAME" => $username);

			$email = new orb_email("Orb upload availability", "no-reply@".$_SERVER["HTTP_HOST"]);
			$email->set_message_fields($fields);
			$email->message($message);
			$email->send($_POST["email"]);
		}

		public function get_upload_list() {
			/* Load uploads
			 */
			if (($uploads = $this->uploads_load()) === false) {
				$this->view->return_error(500);
				return;
			}

			$changed = false;

			foreach ($uploads as $key => $upload) {
				list($directory, $expire, $email, $max_files, $max_size) = explode("|", $upload);

				$timestamp = strtotime($expire);
				if (time() >= $timestamp) {
					unset($uploads[$key]);
					$changed = true;
					continue;
				}

				$expire = date("j F Y", $timestamp);

				$this->view->open_tag("upload", array("key" => $key));
				$this->view->add_tag("directory", $directory);
				$this->view->add_tag("expire", $expire);
				$this->view->add_tag("email", $email);
				$this->view->add_tag("max_files", $max_files);
				$this->view->add_tag("max_size", $max_size);
				$this->view->close_tag();
			}

			if ($changed == false) {
				return;
			}

			$this->uploads_save($uploads);
		}

		public function post_upload_remove() {
			/* Load uploads
			 */
			if (($uploads = $this->uploads_load()) === false) {
				$this->view->return_error(500);
				return;
			}

			if (isset($uploads[$_POST["key"]]) == false) {
				$this->view->return_error(404);
				return;
			}

			unset($uploads[$_POST["key"]]);

			/* Save uploads
			 */
			if ($this->uploads_save($uploads) == false) {
				$this->view->return_error(500);
				return;
			}
		}
	}
?>
