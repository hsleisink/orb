<?xml version="1.0" ?>
<!--
//
//	Copyright (c) by Hugo Leisink <hugo@leisink.net>
//	This file is part of the Orb web desktop
//	https://gitlab.com/hsleisink/orb
//
//	Licensed under the GPLv2 License
//
-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" doctype-system="about:legacy-compat" />

<!--
//
//  Login template
//
-->
<xsl:template match="login">
<div class="content notification">
<form action="" method="post">
<label for="password">Password:</label>
<input type="password" id="password" name="password" autofocus="autofocus" class="form-control" />

<div class="btn-group">
<input type="submit" value="Login" class="btn btn-default" />
</div>
</form>
</div>
</xsl:template>

<!--
//
//  Directory template
//
-->
<xsl:template match="directory">
<div class="content">
<h1><xsl:value-of select="@title" /></h1>
<ul class="directory">
<xsl:for-each select="file">
<li class="{@type}"><a href="{../@base}{@url}"><xsl:value-of select="." /></a></li>
</xsl:for-each>
</ul>
</div>
<div class="generated">Shared via <a href="https://gitlab.com/hsleisink/orb" target="_blank">Orb</a></div>
</xsl:template>

<!--
//
//  Error template
//
-->
<xsl:template match="error">
<div class="content notification">
<h1>Orb file share error</h1>
<p><xsl:value-of select="." /></p>
</div>
</xsl:template>

<!--
//
//  Output template
//
-->
<xsl:template match="/output">
<html lang="en">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="generator" content="File" />
<link rel="apple-touch-icon" href="/images/orb.png" />
<link rel="icon" href="/images/orb.png" />
<link rel="shortcut icon" href="/images/orb.png" />
<title>Orb file share</title>
<xsl:for-each select="styles/style">
<link rel="stylesheet" type="text/css" href="{.}" />
</xsl:for-each>
<xsl:for-each select="javascripts/javascript">
<script type="text/javascript" src="{.}" /><xsl:text>
</xsl:text></xsl:for-each>
</head>

<body>
<xsl:apply-templates select="login" />
<xsl:apply-templates select="directory" />
<xsl:apply-templates select="error" />
</body>

</html>
</xsl:template>

</xsl:stylesheet>
