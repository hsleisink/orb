<?php
	namespace Orb;

	ob_start();

	session_name("Orb");
	$options = array(
		"lifetime" => 0,
		"path"     => "/",
		"domain"   => "",
		"secure"   => true,
		"httponly" => true,
		"samesite" => "none");
	session_set_cookie_params($options);
	session_start();

	chdir("../..");

	require "../libraries/error.php";
	require "../libraries/general.php";
	require "../libraries/setting.php";
	require "../libraries/orb.php";

	/* Dummy view
	 */
	class view_dummy {
		private $view = null;

		public function __construct() {
			$this->view = new view;

			$this->view->open_tag("output");
		}

		public function return_error($message) {
			$this->view->add_tag("error", $message);
			$this->generate();
		}

		public function show_login_form() {
			header("Status: 401");

			$this->view->add_tag("login", "");
			$this->generate();
		}

		public function show_directory($base, $directory, $subdirectory) {
			if (($dp = opendir($directory)) == false) {
				$this->return_error("Directory not found.");
				return;
			}

			$dirs = $files = array();
			while (($file = readdir($dp)) != false) {
				if (substr($file, 0, 1) == ".") {
					continue;
				}

				if (is_dir($directory."/".$file)) {
					array_push($dirs, $file);
				} else {
					array_push($files, $file);
				}
			}

			closedir($dp);

			sort($dirs);
			sort($files);

			if ($subdirectory == "") {
				$parts = explode("/", $directory);
				$title = array_pop($parts);
			} else {
				$title = $subdirectory;
			}

			$this->view->open_tag("directory", array("title" => $title, "base" => $base));

			if ($subdirectory != "") {
				$parts = explode("/", $subdirectory);
				array_pop($parts);
				$back = implode("/", $parts);
				$this->view->add_tag("file", "[ One directory up ]", array("type" => "up", "url" => urlencode($back)));
			}

			foreach ($dirs as $dir) {
				$target = ltrim($subdirectory."/".$dir, "/");
				$this->view->add_tag("file", $dir."/", array("type" => "dir", "url" => urlencode($target)));
			}
			foreach ($files as $file) {
				$target = ltrim($subdirectory."/".$file, "/");
				$this->view->add_tag("file", $file, array("type" => "file", "url" => urlencode($target)));
			}
			$this->view->close_tag();

			$this->generate();
		}

		private function generate() {
			$this->view->add_css("bootstrap.css");
			$this->view->add_css("bootstrap-theme.css");
			$this->view->add_css("notification.css");
			$this->view->add_css("../apps/explorer/share.css");

			$this->view->close_tag();

			ob_end_clean();

			print $this->view->generate("../public/apps/explorer/share");
		}
	}

	/* Share
	 */
	class share extends orb_backend {
		/* Verify authentication
		 */
		private function verify_authentication($share, $password) {
			if (isset($_SESSION["share_access"]) == false) {
				$_SESSION["share_access"] = array();
			}

			if (in_array($share, $_SESSION["share_access"])) {
				return;
			}

			if ($_SERVER["REQUEST_METHOD"] != "POST") {
				$this->view->show_login_form();
				exit;
			}

			if (password_verify($_POST["password"] ?? "", $password) == false) {
				$this->view->show_login_form();
				exit;
			}

			array_push($_SESSION["share_access"], $share);
		}

		/* Execute
		 */
		public function execute() {
			if (valid_input($this->username, VALIDATE_NONCAPITALS, VALIDATE_NONEMPTY) == false) {
				$this->view->return_error("Invalid username.");
				return;
			}

			if (empty($_GET["share"])) {
				$this->view->return_error("No share code specified.");
				return;
			}

			if (isset($_GET["file"])) {
				if ($this->valid_filename($_GET["file"]) == false) {
					$this->view->return_error("Invalid filename.");
					return;
				}
			}

			$shares_file = $this->home_directory."/.file_shares";
			if (file_exists($shares_file) == false) {
				$shares = array();
			} else if (($shares = file_get_contents($shares_file)) == false) {
				$shares = array();
			} else {
				$shares = json_decode($shares, true);
			}

			$share = $shares[$_GET["share"]] ?? null;
			if ($share == null) {
				$this->view->return_error("Share not found.");
				return;
			}

			list($file, $password, $expire, $email) = explode("|", $share);

			if (time() > strtotime($expire)) {
				$this->view->return_error("Share expired.");
				return;
			}

			if ($password != "") {
				$this->verify_authentication($_GET["share"], $password);
			}

			if (isset($_SESSION["notified"]) == false) {
				$this->add_notification($email." accessed '".$file."'.");
				$_SESSION["notified"] = true;
			}

			$target = $this->home_directory."/".$file;
			if (empty($_GET["file"]) == false) {
				$target .= "/".$_GET["file"];
			}

			if (file_exists($target) == false) {
				$this->view->return_error("File not found.");
				return;
			}

			if (is_dir($target)) {
				$base = sprintf("/apps/explorer/share.php?user=%s&share=%s&file=", $this->username, $_GET["share"]);
				$this->view->show_directory($base, $target, $_GET["file"] ?? "");
				return;
			}

			ob_end_clean();

			header("Content-Type: ".get_mimetype($target));
			header("Content-Disposition: inline; filename=\"".basename($target)."\"");
			header("Content-Length: ".filesize($target));
			readfile($target);
		}
	}

	$share = new share(new view_dummy, $_GET["user"] ?? "");
	$share->execute();
?>
