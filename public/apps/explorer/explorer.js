/* Orb Explorer application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const EXPLORER_ICON = '/apps/explorer/explorer.png';

function explorer_set_path(explorer_window, path) {
	explorer_window.find('form input[name=path]').val(path);

	var target = explorer_window.find('div.path');

	target.empty();
	target.append('<a href="">/</a>');

	if (path != '') {
		var parts = path.split('/');
		var url = [];
		parts.forEach(function(part) {
			url.push(part);
			part += '/';
			target.append('<a href="' + url.join('/') + '">' + part + '</a>');
		});
	}

	target.find('a').on('click', function() {
		explorer_update(explorer_window, $(this).attr('href'));
		return false;
	});
}

function explorer_contextmenu_handler(target, option) {
	var explorer_window = target.parent().parent();
	var path = explorer_window.data('path');
	var filename = target.find('span').first().text();

	switch (option) {
		case 'Download':
			var url = path;
			if (path != '') {
				url += '/';
			}
			url += filename;
			url = orb_download_url(url);

			window.open(url, '_blank').focus();
			break;
		case 'Rename':
			orb_prompt('Rename file:', filename, function(new_filename) {
				new_filename = new_filename.trim();
				if (new_filename == '') {
					orb_alert('The new filename cannot be empty.');
				} else if (new_filename != filename) {
					orb_file_rename(path + '/' + filename, new_filename, undefined, function() {
						orb_alert('Error while renaming file or directory.', 'Error');
					});
				}
			});
			break;
		case 'Create link':
			orb_file_dialog('Link', function(link) {
				orb_file_link(path + '/' + filename, link, undefined, function() {
					orb_alert('Error creating link.', 'Error');
				});
			}, 'Desktop', filename);
			break;
		case 'Link info':
			var target = target.find('span').first().attr('target');
			orb_alert('Link to: ' + target);
			break;
		case 'Open in new window':
			if (path != '') {
				filename = path + '/' + filename;
			}
			explorer_open(filename);
			break;
		case 'Share':
			var share = filename;
			if (path != '') {
				share = path + '/' + share;
			}

			var dialog = '<div class="explorer_share">' +
				'<div class="form-group">' +
				'<label>Share:</label>' +
				'<div>' + share + '</div>' +
				'</div><div class="form-group">' +
				'<label for="email">E-mail address:</label>' +
				'<input type="text" id="email" class="form-control" />' +
				'</div><div class="form-group">' +
				'<label for="email">Password:</label>' +
				'<input type="password" id="password" placeholder="optional" class="form-control" />' +
				'</div><div class="form-group">' +
				'<label for="expire">Expire after:</label>' +
				'<select id="expire" class="form-control">' +
				'<option value="1">This day</option>' +
				'<option value="3">Three days</option>' +
				'<option value="7">One week</option>' +
				'<option value="21">Three weeks</option>' +
				'<option value="70">Ten weeks</option>' +
				'<option value="365">One year</option>' +
				'</select>' +
				'</div><div class="form-group">' +
				'<label for="comment">Comment:</label>' +
				'<input type="text" id="comment" placeholder="optional" class="form-control" />' +
				'</div><div class="btn-group">' +
				'<button class="btn btn-default share">Share</button>' +
				'<button class="btn btn-default cancel">Cancel</button>' +
				'</div></div>';
			var share_window = $(dialog).orb_window({
				header: 'File sharing',
				icon: EXPLORER_ICON,
				width: 400,
				dialog: true,
				maximize: false,
				resize: false,
				open: function() {
					share_window.find('input#email').focus();
				},
				close: function() {
					$(document).off('keydown', key_handler);
				}
			});

			share_window.find('button.share').on('click', function(event) {
				share_window.find('.has-error').removeClass('has-error');

				var email = share_window.find('input#email').val().trim().toLowerCase();
				if (email.match(/^(.+)@(.+)\.([a-z]{2,})$/) == null) {
					share_window.find('input#email').parent().addClass('has-error');
					share_window.find('input#email').focus();
					return;
				}

				$.post('/explorer/share_create', {
					filename: share,
					email: share_window.find('input#email').val(),
					password: share_window.find('input#password').val(),
					expire: share_window.find('select#expire').val(),
					comment: share_window.find('input#comment').val()
				}).done(function() {
					share_window.close();
				}).fail(function(result) {
					orb_alert('Error sharing file.');
				});
				event.stopPropagation();
			});

			share_window.find('button.cancel').on('click', function() {
				share_window.close();
			});

			share_window.open();

			var key_handler = function(event) {
				if (event.which == 27) {
					share_window.find('div.btn-group button.cancel').trigger('click');
				}
			};
			$(document).on('keydown', key_handler);
			break;
		case 'Uploading':
			var directory = filename;
			if (path != '') {
				directory = path + '/' + directory 
			}

			var dialog = '<div class="explorer_upload">' +
				'<div class="form-group">' +
				'<label>Upload directory:</label>' +
				'<div>' + directory + '</div>' +
				'</div><div class="form-group">' +
				'<label for="email">E-mail address:</label>' +
				'<input type="text" id="email" class="form-control" />' +
				'</div><div class="form-group">' +
				'<label for="max_files">Maximum number of files:</label>' +
				'<input type="text" id="max_files" value="5" class="form-control" />' +
				'</div><div class="form-group">' +
				'<label for="max_size">Maximum total file size:</label>' +
				'<div class="input-group"><input type="text" id="max_size" value="20" class="form-control" /><span class="input-group-addon">MB</span></div>' +
				'</div><div class="form-group">' +
				'<label for="expire">Expire after:</label>' +
				'<select id="expire" class="form-control">' +
				'<option value="1">This day</option>' +
				'<option value="2">Two days</option>' +
				'<option value="3">Three days</option>' +
				'<option value="7">One week</option>' +
				'</select>' +
				'</div><div class="form-group">' +
				'<label for="comment">Comment:</label>' +
				'<input type="text" id="comment" placeholder="optional" class="form-control" />' +
				'</div><div class="btn-group">' +
				'<button class="btn btn-default send">Send</button>' +
				'<button class="btn btn-default cancel">Cancel</button>' +
				'</div></div>';
			var upload_window = $(dialog).orb_window({
				header: 'Make available for upload',
				icon: EXPLORER_ICON,
				width: 400,
				dialog: true,
				maximize: false,
				resize: false,
				open: function() {
					upload_window.find('input#email').focus();
				},
				close: function() {
					$(document).off('keydown', key_handler);
				}
			});

			upload_window.find('button.send').on('click', function(event) {
				upload_window.find('.has-error').removeClass('has-error');

				var email = upload_window.find('input#email').val().trim().toLowerCase();
				if (email.match(/^(.+)@(.+)\.([a-z]{2,})$/) == null) {
					upload_window.find('input#email').parent().addClass('has-error');
					upload_window.find('input#email').focus();
					return;
				}

				$.post('/explorer/upload_create', {
					directory: directory,
					email: upload_window.find('input#email').val(),
					max_files: upload_window.find('input#max_files').val(),
					max_size: upload_window.find('input#max_size').val(),
					expire: upload_window.find('select#expire').val(),
					comment: upload_window.find('input#comment').val()
				}).done(function() {
					upload_window.close();
				}).fail(function(result) {
					orb_alert('Error sharing file.');
				});
				event.stopPropagation();
			});

			upload_window.find('button.cancel').on('click', function() {
				upload_window.close();
			});

			upload_window.open();

			var key_handler = function(event) {
				if (event.which == 27) {
					upload_window.find('div.btn-group button.cancel').trigger('click');
				}
			};
			$(document).on('keydown', key_handler);
			break;
		case 'Delete':
			orb_confirm('Delete ' + filename + '?', function() {
				if (path != '') {
					filename = path + '/' + filename;
				}

				if (target.attr('type') == 'file') {
					orb_file_remove(filename, undefined, function() {
						orb_alert('Error while deleting file.', 'Error');
					});
				} else {
					orb_directory_remove(filename, undefined, function() {
						orb_alert('Error while deleting directory.', 'Error');
					});
				}
			});
			break;
	}
}

/* File icon
 */
function explorer_entry_add(files, format, item, path) {
	if (item.type == 'directory') {
		var image = '/images/directory.png';
		extension = '';
	} else {
		var extension = orb_file_extension(item.name);
		if (extension != false) {
			var image = orb_get_file_icon(extension);
		} else {
			var image = '/images/file.png';
			extension = '';
		}
	}

	var icon = '<div class="entry ' + format + '" type="' + item.type + '" ext="' +
		extension + '" link="' + (item.link ? 'yes' : 'no') + '">' +
		'<img src="' + image + '" alt="' + item.name + '" draggable="false" />' +
		(item.link ? '<img src="/images/link.png" draggable="false" />' : '') +
		'<span path="' + path + '" type="' + item.type + '"';
	if (item.create != undefined) {
		icon += ' create="' + item.create + '"';
	}
	if (item.access != undefined) {
		icon += ' access="' + item.access + '"';
	}
	if (item.target != undefined) {
		icon += ' target="' + item.target + '"';
	}
	icon += '>' + item.name + '</span>';
	if ((item.type == 'file') && (item.size != undefined)) {
		icon += '<span number="' + item.size + '">';
		icon += orb_file_nice_size(item.size);
	} else {
		icon += '<span>&nbsp;';
	}

	if ($('div.desktop').attr('mobile') == 'yes') {
		icon += '</span><span class="glyphicon glyphicon-chevron-down" aria-hidden="true">';
	}

	icon += '</span></div>';

	files.append(icon);
}

function explorer_update(explorer_window, path = undefined) {
	if (path == undefined) {
		path = explorer_window.data('path');
	}

	orb_directory_list(path, function(items) {
		explorer_set_path(explorer_window, path);

		explorer_window.data('path', path);

		var information = explorer_window.find('div.information');
		var files = explorer_window.find('div.files');
		var filename = explorer_window.find('input.filename');
		var format = explorer_window.data('format');

		information.empty();
		files.empty();
		filename.val('');

		/* Fill explorer
		 */
		items.forEach(function(item) {
			if (item.type == 'directory') {
				explorer_entry_add(files, format, item, path);
			}
		});
		var dir_count = files.find('div').length;

		items.forEach(function(item) {
			if (item.type == 'file') {
				explorer_entry_add(files, format, item, path);
			}
		});
		var file_count = files.find('div').length - dir_count;
		
		files.scrollTop(0);

		information.append('<b style="display:block; margin-top:30px">Content:</b>');
		information.append('<p>' + dir_count + ' directories<br />' + file_count + ' files</p>');

		/* Mobile
		 */
		if ($('div.desktop').attr('mobile') == 'yes') {
			explorer_window.find('div.files div.detail span:nth-of-type(2)').css('right', '25px');
			explorer_window.find('div.files div.detail span:nth-of-type(3)').css('display', 'inline');
			explorer_window.find('div.files div.detail span:nth-of-type(3)').on('click', function(event) {
				var e = jQuery.Event('contextmenu');
				e.clientX = event.clientX;
				e.clientY = event.clientY;
				$(this).parent().trigger(e);

				event.stopPropagation();
			});
		}

		/* Double-click directory
		 */
		explorer_window.find('div.files div.entry[type=directory]').on('dblclick', function() {
			explorer_window.find('div.files div.entry[type=directory]').off('dblclick');

			var dir = $(this).find('span').first().text();

			if (path == '') {
				path = dir;
			} else {
				path += '/' + dir;
			}

			explorer_update(explorer_window, path);
		});

		/* Double-click file
		 */
		explorer_window.find('div.files div.entry[type=file]').on('dblclick', function() {
			var filename = path + '/' + $(this).find('span').first().text();
			var extension = orb_file_extension(filename);

			if ((handler = orb_get_file_handler(extension)) != undefined) {
				handler(filename);
			} else {
				window.open(orb_download_url(filename), '_blank').focus();
			}
		});

		/* Click file
		 */
		explorer_window.find('div.files div.entry').on('click', function(event) {
			var name = $(this).find('span').first().text();

			if ($('div.desktop').attr('mobile') == 'yes') {
				if (explorer_window.data('click_last') == name) {
					explorer_window.data('click_last', null);
					$(this).trigger('dblclick');

					event.stopPropagation();
				} else {
					explorer_window.data('click_last', name);
				}
			}

			//if (orb_key_pressed(KEY_CTRL) == false) {
				explorer_window.find('div.files div.selected').removeClass('selected');
			//}
			$(this).addClass('selected');

			var information = explorer_window.find('div.information');
			information.empty();

			var src = $(this).find('img').attr('src');
			information.append('<img src="' + src + '" />');

			information.append('<p><b>File name:</b><br />' + name + '</p>');

			var create = $(this).find('span').first().attr('create');
			if ((create != undefined) && (create != '')) {
				var parts = create.split(', ');
				information.append('<p><b>Creation date:</b><br />' + parts[0] + '<br />' + parts[1] + '</p>');
			}

			var access = $(this).find('span').first().attr('access');
			if ((access != undefined) && (access != '')) {
				var parts = access.split(', ');
				information.append('<p><b>Last access date:</b><br />' + parts[0] + '<br />' + parts[1] + '</p>');
			}

			var type = $(this).find('span').first().attr('type');
			if (type == 'file') {
				var size = $(this).find('span').last().attr('number');
				if (size != null) {
					information.append('<p><b>File size:</b><br />' + size + '</p>');
				}
			}
		});

		/* Right-click file
		 */
		var window_id = explorer_window.parent().parent().attr('id');

		$('div#' + window_id + ' div.files div.entry').on('contextmenu', function(event) {
			explorer_window.find('div.files div.selected').removeClass('selected');
			$(this).addClass('selected');
			
			var menu_entries = [];

			if ($(this).attr('type') == 'file') {
				menu_entries.push({ name: 'Download', icon: 'download' });
			} else {
				menu_entries.push({ name: 'Open in new window', icon: 'window-restore' });
			}

			if (orb_read_only == false) {
				menu_entries.push({ name: 'Rename', icon: 'edit' });

				if ($(this).attr('link') == 'yes') {
					menu_entries.push({ name: 'Link info', icon: 'info-circle' });
				} else {
					menu_entries.push({ name: 'Create link', icon: 'chain' });
				}
			}

			var login = $('div.desktop').attr('login');
			if (login != 'none') {
				menu_entries.push({ name: 'Share', icon: 'upload' });
				if ($(this).attr('type') == 'directory') {
					menu_entries.push({ name: 'Uploading', icon: 'download' });
				}
			}

			if (orb_read_only == false) {
				menu_entries.push({ name: 'Delete', icon: 'remove' });
			}

			if (menu_entries.length > 0) {
				orb_contextmenu_add_items(menu_entries, $(this).attr('ext'));
				orb_contextmenu_show($(this), event, menu_entries, explorer_contextmenu_handler);
			}

			return false;
		});

		if (orb_read_only == false) {
			explorer_window.find('div.files > div.entry').draggable({
				appendTo: 'div.icons',
				helper: 'clone',
				zIndex: 10000,
				start: function(event, ui) {
					orb_window_raise(explorer_window.parent().parent());

					if (ui.helper.hasClass('detail')) {
						ui.helper.removeClass('detail');
						ui.helper.addClass('icon');
					}
				}
			});
		}

		if (($('div.desktop').attr('mobile') == 'yes') && ($('input.drag').prop('checked') == false)) {
			explorer_window.find('div.files > div.entry').draggable({
				disabled: true
			});
		}

		$('button.directory_up').prop('disabled', false);
	}, function(result) {
		if (result == 401) {
			explorer_window.close();
			alert('Login has been expired. No access to disk.');
			orb_logout(true);
		} else if (path != '') {
			orb_alert('Error reading directory.', 'Error');
			explorer_update(explorer_window, '');
		}
	});
}

function explorer_set_format(explorer_window, format) {
	if (explorer_window.data('format') != format) {
		explorer_window.data('format', format);
		orb_setting_set('applications/explorer/format', format);
		explorer_update(explorer_window);
	}

	explorer_update_menu(explorer_window);
}

function explorer_update_menu(explorer_window) {
	var menu = explorer_window.parent().parent().find('ul.nav li:nth-child(2) ul');

	menu.find('li').removeClass('selected');

	if (explorer_window.data('format') == 'icon') {
		menu.find('li:nth-child(1)').addClass('selected');
	} else {
		menu.find('li:nth-child(2)').addClass('selected');
	}
}

function explorer_menu_click(explorer_window, item) {
	switch (item) {
		case 'New Explorer window':
			var path = explorer_window.data('path');
			explorer_open(path);
			break;
		case 'New directory':
			orb_prompt('New directory name:', '', function(directory) {
				directory = directory.trim();
				if (directory == '') {
					return;
				} else if (directory.indexOf('/') != -1) {
					return;
				}

				var path = explorer_window.data('path');
				orb_directory_create(path + '/' + directory, undefined, function() {
					orb_alert('Error while creating directory.', 'Error');
				});
			});
			break;
		case 'New file':
			orb_prompt('New file name:', '', function(filename) {
				filename = filename.trim();
				if (filename == '') {
					return;
				} else if (filename.indexOf('/') != -1) {
					return;
				}

				var path = explorer_window.data('path');
				orb_file_save(path + '/' + filename, '', false, undefined, function() {
					orb_alert('Error while creating file.', 'Error');
				});
			});
			break;
		case 'Download remote file':
			orb_prompt('URL of the remote file:', '', function(url) {
				var path = explorer_window.data('path');
				var parts = url.split('?');

				$.post('/explorer/download', {
					url: url,
					path: path
				}).done(function() {
					explorer_update(explorer_window);
					if (path == 'Desktop') {
						orb_desktop_refresh();
					}
					orb_alert('Download complete.');
				}).fail(function(xhr) {
					if (xhr.status == 409) {
						orb_alert('File already exists.');
					} else {
						orb_alert('Download failed.');
					}
				});
			});
			break;
		case 'Exit':
			explorer_window.close();
			break;
		case 'Icons':
			explorer_set_format(explorer_window, 'icon');
			break;
		case 'Details':
			explorer_set_format(explorer_window, 'detail');
			break;
		case 'File shares':
			$.ajax({
				url: '/explorer/share_list'
			}).done(function(data) {
				var shares = '<div class="explorer_shares"><table class="table table-condensed table-striped">' +
					'<thead><tr><th>File</th><th>Expires</th><th>Shared with</th><th>Pwd</th><th></th></tr></thead>' +
					'<tbody>';
				$(data).find('share').each(function() {
					shares += '<tr key="' + $(this).attr('key') + '">' +
						'<td>' + $(this).find('file').text() + '</td>' +
						'<td>' + $(this).find('expire').text() + '</td>' +
						'<td>' + $(this).find('email').text() + '</td>' +
						'<td>' + $(this).find('password').text() + '</td>' +
						'<td><button class="btn btn-danger btn-xs">X</button></tr>';
				});
				shares += '</tbody></table></div>';

				var shares_window = $(shares).orb_window({
					header: 'File shares',
					icon: EXPLORER_ICON,
					width: 700,
					dialog: true,
					maximize: false,
					resize: false,
					close: function() {
						$(document).off('keydown', key_handler);
					}
				});

				shares_window.find('button.btn').on('click', function() {
					if (confirm('DELETE: Are you sure?')) {
						var share = $(this).parent().parent();

						$.post('/explorer/share_remove',	{
							key: share.attr('key')
						}).done(function() {
							share.remove();
						}).fail(function() {
							orb_alert('Error unsharing file.');
						});
					}
				});

				shares_window.open();

				var key_handler = function(event) {
					if (event.which == 27) {
						shares_window.close();
					}
				};
				$(document).on('keydown', key_handler);
			}).fail(function(result) {
				orb_alert('Error loading shares.');
			});
			break;
		case 'Upload directories':
			$.ajax({
				url: '/explorer/upload_list'
			}).done(function(data) {
				var uploads = '<div class="explorer_uploads"><table class="table table-condensed table-striped">' +
					'<thead><tr><th>Directory</th><th>Expires</th><th>Uploading by</th><th>Limits</th><th></th></tr></thead>' +
					'<tbody>';
				$(data).find('upload').each(function() {
					uploads += '<tr key="' + $(this).attr('key') + '">' +
						'<td>' + $(this).find('directory').text() + '</td>' +
						'<td>' + $(this).find('expire').text() + '</td>' +
						'<td>' + $(this).find('email').text() + '</td>' +
						'<td>' + $(this).find('max_files').text() + ' / ' + $(this).find('max_size').text() + ' MB</td>' +
						'<td><button class="btn btn-danger btn-xs">X</button></tr>';
				});
				uploads += '</tbody></table></div>';

				var uploads_window = $(uploads).orb_window({
					header: 'Upload directories',
					icon: EXPLORER_ICON,
					width: 700,
					dialog: true,
					maximize: false,
					resize: false,
					close: function() {
						$(document).off('keydown', key_handler);
					}
				});

				uploads_window.find('button.btn').on('click', function() {
					if (confirm('DELETE: Are you sure?')) {
						var upload = $(this).parent().parent();

						$.post('/explorer/upload_remove',	{
							key: upload.attr('key')
						}).done(function() {
							upload.remove();
						}).fail(function() {
							orb_alert('Error unsharing file.');
						});
					}
				});

				uploads_window.open();

				var key_handler = function(event) {
					if (event.which == 27) {
						uploads_window.close();
					}
				};
				$(document).on('keydown', key_handler);
			}).fail(function(result) {
				orb_alert('Error loading upload directories.');
			});
			break;
		case 'By filename':
			orb_prompt('Search for (part of) filename:', '', function(filename) {
				var path = explorer_window.data('path');

				orb_file_search(filename, path, function(data) {
					var search_result = '<div class="search_result"><h1>Search results for \'' + filename +
						'\'</h1><div class="path">Search root: /' + path + '</div><div class="hits">';

					data.forEach(function(item) {
						if (item.type == 'dir') {
							item.filename += '/';
							var image = '/images/directory.png';
						} else {
							var extension = orb_file_extension(item.filename);
							if (extension != false) {
								var image = orb_get_file_icon(extension);
							} else {
								var image = '/images/file.png';
								extension = '';
							}
						}

						search_result += '<div type="' + item.type + '"><img src="' + image + '" /><span>' + item.filename + '</span></div>';
					});

					search_result += '</div>';

					var search_result_window = $(search_result).orb_window({
						header:'Search results',
						icon: EXPLORER_ICON,
						width: 700,
						height: 400
					});

					/* Click directory
					 */
					search_result_window.find('div.hits div[type=dir]').on('click', function(event) {
						event.stopPropagation();

						var dir = $(this).find('span').text();
						dir = dir.replace(/\/+$/, '');

						explorer_open(dir);
					});

					/* Click file
					 */
					search_result_window.find('div.hits div[type=file]').on('click', function(event) {
						event.stopPropagation();

						var filename = $(this).find('span').text();
						var extension = orb_file_extension(filename);

						if ((handler = orb_get_file_handler(extension)) != undefined) {
							handler(filename);
						} else {
							window.open(orb_download_url(filename), '_blank').focus();
						}
					});

					search_result_window.open();
				}, function() {
					orb_alert('Search query too short.', 'Search error');
				});
			});
			break;
		case 'About':
			orb_alert('<img src="' + EXPLORER_ICON + '" class="about" draggable="false" />File explorer\nCopyright (c) by Hugo Leisink', 'About');
			break;
	}
}

function explorer_open(directory = undefined) {
	var dropzone_id = 0
	while ($('div.explorer form#dropzone' + dropzone_id).length > 0) {
		dropzone_id++;
	}

	var window_content =
		'<div class="explorer">' +
		'<button class="btn btn-default btn-xs refresh"><span class="fa fa-refresh"></span></button>' +
		'<button class="btn btn-default btn-xs directory_up"><span class="fa fa-chevron-up"></span></button>' +
		'<div class="path"></div>' +
		'<div class="information"></div>' +
		'<div class="files"></div>' +
		'<form action="/explorer" class="dropzone" id="dropzone' + dropzone_id + '">' +
		'<div class="dz-message"><span>Drop files here to upload them to the current folder.</span></div>' +
		'<input type="hidden" name="path" value="" />' +
		'</form>' +
		'</div>';

	if (orb_read_only) {
		var menu_file = [ 'New Explorer window', 'Exit' ];
	} else {
		var menu_file = [ 'New Explorer window', 'New directory', 'New file', 'Download remote file', '-', 'Exit' ];
	}

	var explorer_window = $(window_content).orb_window({
		header:'Explorer',
		icon: EXPLORER_ICON,
		width: 760,
		height: 430,
		menu: {
			'File': menu_file,
			'View': [ 'Icons', 'Details', '-', 'File shares', 'Upload directories' ],
			'Search': [ 'By filename' ],
			'Help': [ 'About' ]
		},
		menuCallback: explorer_menu_click
	});

	if ($('div.desktop').attr('mobile') == 'yes') {
		explorer_window.parent().parent().css('min-height', '390px');
	} else {
		explorer_window.parent().parent().css('min-height', '270px');
	}

	explorer_window.open();

	if (directory == undefined) {
		directory = '';
	} else {
		directory = directory.replace(/^\/+/, '');
	}

	explorer_window.data('path', directory);

	orb_setting_get('applications/explorer/format', function(value) {
		explorer_window.data('format', value);
		explorer_update(explorer_window);
		explorer_update_menu(explorer_window);
	}, function() {
		explorer_window.data('format', 'icon');
		explorer_update(explorer_window);
		orb_setting_set('applications/explorer/format', 'icon');
		explorer_update_menu(explorer_window);
	});

	explorer_window.find('button.directory_up').on('click', function() {
		var path = explorer_window.data('path');

		if (path == '') {
			return;
		}

		var parts = path.split('/');
		parts.pop();
		path = parts.join('/');

		$(this).prop('disabled', true);
		explorer_update(explorer_window, path);
	});

	explorer_window.find('button.refresh').on('click', function() {
		explorer_update(explorer_window);
	});

	if (orb_read_only == false) {
		explorer_window.droppable({
			accept: 'div.icon, div.detail',
			greedy: true,
			over: function() {
				var current = $(this);
				$('div.windows div.explorer').each(function() {
					if ($(this).is(current) == false) {
						$(this).droppable('disable');
					}
				});

				orb_window_raise(explorer_window.parent().parent());
			},
			out: function() {
				$('div.windows div.explorer').droppable('enable');
			},
			drop: function(event, ui) {
				event.stopPropagation();

				var span = ui.helper.find('span').first();
				var source_path = span.attr('path');
				var source = source_path + '/' + span.text();
				var source_filename = orb_file_filename(source);
				var destination_path = explorer_window.data('path');

				if (source_path == destination_path) {
					if (source_path == 'Desktop') {
						orb_desktop_refresh();
					}
					return;
				}

				orb_file_exists(destination_path + '/' + source_filename, function(exists) {
					var ctrl_pressed = orb_key_pressed(KEY_CTRL);

					var file_operation = function() {
						if (ctrl_pressed) {
							orb_file_copy(source, destination_path, undefined, function() {
								orb_alert('Error copying file.', 'Error');
							});
						} else {
							orb_file_move(source, destination_path, undefined, function() {
								orb_alert('Error moving file.', 'Error');
							});
						}
					};

					if (exists) {
						orb_confirm('Destination file already exists. Overwrite?', file_operation, function() {
							if ((ctrl_pressed == false) && (orb_file_dirname(source) == _orb_desktop_path)) {
								orb_desktop_refresh();
							}
						});
					} else {
						file_operation();
					}
				});

				$('div.windows div.explorer').droppable('enable');
			}
		});

		/* Dropzone
		 */
		var dropzone_visible = false;
		var files_dropped = 0;

		Dropzone.options['dropzone' + dropzone_id] = {
			init: function() {
				explorer_window.parent().parent().append($('body input.dz-hidden-input').first());

				this.on('drop', function(data) {
					files_dropped += data.dataTransfer.files.length;
				});

				this.on('success', function(file) {
					var path = explorer_window.data('path');
					orb_directory_notify_update(path);
				});

				this.on('complete', function(file) {
					files_dropped--;

					var dropzone = this;
					setTimeout(function() {
						dropzone.removeFile(file)

						if (files_dropped == 0) {
							explorer_window.find('form.dropzone').hide();
							dropzone_visible = false;
						}
					}, 3000);
				});

				this.on('error', function(file, data) {
					if (data.substr(0, 5) == '<?xml') {
						var error = parseInt($(data).find('error').text());
						switch (error) {
							case 400:
								orb_alert('Error uploading file.');
								break;
							case 403:
								orb_alert('The maximum of your storage capacity has been reached.');
								break;
							case 404:
								orb_alert('Error uploading file to the specified destination.');
								break;
						}
					} else {
						orb_alert(data);
					}
				});
			}
		};

		var dropzone = new Dropzone('#dropzone' + dropzone_id, {
			url: '/explorer'
		});

		explorer_window.on('dragover', function(event) {
			event.preventDefault();
			event.stopPropagation();

			if (dropzone_visible == false) {
				dropzone_visible = true;
				explorer_window.find('form.dropzone').show();

				setTimeout(function() {
					if (files_dropped == 0) {
						explorer_window.find('form.dropzone').hide();
						dropzone_visible = false;
					}
				}, 3000);
			}
		});
	}

	/* Mobile support
	 */
	if ($('div.desktop').attr('mobile') == 'yes') {
		explorer_window.find('div.information').css('width', '150px');
		explorer_window.find('div.information').css('font-size', '12px');
		explorer_window.find('div.files').css('left', '165px');
		explorer_window.find('form.dropzone').css('left', '165px');

		explorer_window.find('form.dropzone').css('bottom', '30px');

		explorer_window.find('div.information').css('bottom', '25px');
		if (orb_read_only == false) {
			var drag = $('<div class="drag"><input type="checkbox" class="drag" />Icons draggable</div>');
			drag.find('input').on('click', function() {
				var checked = $(this).prop('checked');
				explorer_window.find('div.files > div.entry').draggable({
					disabled: checked == false
				});
			});
			explorer_window.append(drag);

			var upload = $('<div class="upload"><input type="checkbox" class="upload" />Upload element</div>');
			upload.find('input').on('click', function() {
				if ($(this).prop('checked')) {
					explorer_window.find('form.dropzone').show();
				} else {
					explorer_window.find('form.dropzone').hide();
				}
			});
			explorer_window.append(upload);
		}

		explorer_window.find('div.files').css('bottom', '30px');
		var scroll = $('<div class="scroll btn-group"><button class="btn btn-default btn-xs scroll_up"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></button><button class="btn btn-default btn-xs scroll_down"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></button></div>');
		scroll.find('button.scroll_up').on('click', function() {
			var current = explorer_window.find('div.files').scrollTop();
			explorer_window.find('div.files').scrollTop(current - 88);
		});
		scroll.find('button.scroll_down').on('click', function() {
			var current = explorer_window.find('div.files').scrollTop();
			explorer_window.find('div.files').scrollTop(current + 88);
		});
		explorer_window.append(scroll);
	}
}

$(document).ready(function() {
	if ($('div.desktop').attr('read_only') == 'no') {
		orb_load_javascript('/apps/explorer/dropzone.js');
		orb_load_stylesheet('/apps/explorer/dropzone.css');
	}

	orb_startmenu_add('Explorer', EXPLORER_ICON, explorer_open);
	orb_quickstart_add('Explorer', EXPLORER_ICON, explorer_open);
	orb_upon_directory_open(explorer_open);

	orb_directory_upon_update(function(directory) {
		$('div.windows div.explorer').each(function() {
			if ($(this).data('path') == directory) {
				explorer_update($(this));
			}
		});
	});
});
