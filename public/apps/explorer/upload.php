<?php
	namespace Orb;

	ob_start();

	session_name("Orb");
	$options = array(
		"lifetime" => 0,
		"path"     => "/",
		"domain"   => "",
		"secure"   => true,
		"httponly" => true,
		"samesite" => "none");
	session_set_cookie_params($options);
	session_start();

	chdir("../..");

	require "../libraries/error.php";
	require "../libraries/general.php";
	require "../libraries/setting.php";
	require "../libraries/orb.php";

	/* Dummy view
	 */
	class view_dummy {
		private $view = null;

		public function __construct() {
			$this->view = new view;

			$this->view->open_tag("output");
		}

		public function return_error($message) {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				header("Status: ".(is_int($message) ? (string)$message : "500"));
			} else {
				$this->view->add_tag("error", $message);
				$this->generate();
			}
		}

		public function show_upload($username, $key) {
			$this->view->add_css("../apps/explorer/dropzone.css");

			$this->view->add_javascript("jquery.js");
			$this->view->add_javascript("../apps/explorer/dropzone.js");
			$this->view->add_javascript("../apps/explorer/upload.js");

			$this->view->open_tag("upload");
			$this->view->add_tag("username", $username);
			$this->view->add_tag("key", $key);
			$this->view->close_tag();

			$this->generate();
		}

		private function generate() {
			$this->view->add_css("bootstrap.css");
			$this->view->add_css("bootstrap-theme.css");
			$this->view->add_css("notification.css");
			$this->view->add_css("../apps/explorer/upload.css");

			$this->view->close_tag();

			ob_end_clean();

			print $this->view->generate("../public/apps/explorer/upload");
		}
	}

	/* Upload 
	 */
	class upload extends orb_backend {
		/* Upload file via drag & drop
		 */
		private function handle_upload($directory, $max_files, $max_size) {
			if (is_array($_FILES["file"] ?? null) == false) {
				$this->view->return_error(400);
				return;
			}

			if ($_FILES["file"]["error"] != 0) {
				$this->view->return_error(400);
				return;
			}

			/* Check capacity
			 */
			$users = file(PASSWORD_FILE);

			$capacity = null;
			foreach ($users as $user) {
				$parts = explode(":", trim($user));

				if ($parts[0] != $this->username) {
					continue;
				} else if (count($parts) < 3) {
					continue;
				} else if ($parts[2] == "") {
					continue;
				}

				$capacity = (int)$parts[2];
			}

			if ($capacity !== null) {
				$total_size = $this->directory_size($this->home_directory);

				if ($total_size + (int)$_FILES["file"]["size"] > $capacity * MB) {
					$this->view->return_error(403);
					return;
				}
			}

			$destination = $this->home_directory."/".$directory."/";

			if (($dp = opendir($destination)) == false) {
				$this->view->return_error(404);
				return;
			}

			$file_count = 0;
			$file_size = (int)$_FILES["file"]["size"];

			while (($file = readdir($dp)) !== false) {
				if (substr($file, 0, 1) == ".") {
					continue;
				} else if (is_dir($destination.$file)) {
					continue;
				}

				$file_count++;
				$file_size += filesize($destination.$file);
			}

			closedir($dp);

			if (($file_count >= $max_files) || ($file_size >= $max_size * MB)) {
				$this->view->return_error(403);
				return;
			}

			/* Move file
			 */
			if (file_exists($destination.$_FILES["file"]["name"])) {
				$pathinfo = pathinfo($_FILES["file"]["name"]);
				$counter = 1;

				do {
					$test = $destination.$pathinfo["filename"]." (".$counter.").".$pathinfo["extension"];
					$counter++;
				} while (file_exists($test));

				$destination = $test;
			} else {
				$destination .= $_FILES["file"]["name"];
			}

			move_uploaded_file($_FILES["file"]["tmp_name"], $destination);
		}

		/* Execute
		 */
		public function execute() {
			if (is_true(READ_ONLY)) {
				$this->view->return_error(403);
				return;
			}

			if (valid_input($this->username, VALIDATE_NONCAPITALS, VALIDATE_NONEMPTY) == false) {
				$this->view->return_error("Invalid username.");
				return;
			}

			if (empty($_GET["upload"])) {
				$this->view->return_error("No upload access code specified.");
				return;
			}

			$uploads_file = $this->home_directory."/.upload_directories";
			if (file_exists($uploads_file) == false) {
				$uploads = array();
			} else if (($uploads = file_get_contents($uploads_file)) == false) {
				$uploads = array();
			} else {
				$uploads = json_decode($uploads, true);
			}

			$upload = $uploads[$_GET["upload"]] ?? null;
			if ($upload == null) {
				$this->view->return_error("Upload not found.");
				return;
			}

			list($directory, $expire, $email, $max_files, $max_size) = explode("|", $upload);

			if (time() > strtotime($expire)) {
				$this->view->return_error("Upload expired.");
				return;
			}

			if (is_dir($this->home_directory."/".$directory) == false) {
				$this->view->return_error("Upload directory not found.");
				return;
			}

			ob_end_clean();

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$this->handle_upload($directory, $max_files, $max_size);

				$this->add_notification($email." uploaded '".$_FILES["file"]["name"]."' to '".$directory."'.");
			} else {
				$this->view->show_upload($this->username, $_GET["upload"]);
			}
		}
	}

	$upload = new upload(new view_dummy, $_GET["user"] ?? "");
	$upload->execute();
?>
