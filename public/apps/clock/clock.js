/* Orb Clock application
 *
 * Copyright (c) by Hugo Leisink <hugo@leisink.net>
 * This file is part of the Orb web desktop
 * https://gitlab.com/hsleisink/orb
 *
 * Licensed under the GPLv2 License
 */

const CLOCK_ICON = '/apps/clock/clock.png';

const CLOCK_WIDTH = 0.075;
const CLOCK_TAIL = 1.5;

const CLOCK_MODE_TIME = 0;
const CLOCK_MODE_STOPWATCH = 1;
const CLOCK_MODE_COUNTDOWN = 2;

const CLOCK_STATUS_RESET = 0;
const CLOCK_STATUS_RUNNING = 1;
const CLOCK_STATUS_PAUSED = 2;
const CLOCK_STATUS_STOPPED = 3;

function clock_mode_countdown(clock_window) {
	orb_prompt('Specify start time:', '00:15:00', function(result) {
		var time = result.split(':');
		if (time.length != 3) {
			return;
		}

		time[2] = parseInt(time[2]);
		time[1] = parseInt(time[1]);
		time[0] = parseInt(time[0]);

		if (isNaN(time[0]) || isNaN(time[1]) || isNaN(time[2])) {
			return;
		} else if ((time[0] < 0) || (time[1] < 0) || (time[2] < 0)) {
			return;
		} else if ((time[0] == 0) && (time[1] == 0) && (time[2] == 0)) {
			return;
		}

		var seconds = time[2] % 60;
		time[1] += Math.floor(time[2] / 60);
		var minutes = time[1] % 60;
		time[0] += Math.floor(time[1] / 60);
		var hours = time[0] % 100;

		clock_window.data('time', [ hours, minutes, seconds ]);

		clock_window.data('mode', CLOCK_MODE_COUNTDOWN);
		clock_window.data('status', CLOCK_STATUS_RESET);
		clock_show_time(clock_window);

		clock_window.set_header('Countdown');
	});
}

function clock_resize(clock_window) {
	var width = Math.round(clock_window.parent().width());
	var height = Math.round(clock_window.parent().height());

	clock_window.find('div.digital').css('font-size', Math.round(width / 3.8) + 'px');

	if (width > height) {
		width = height;
	} else {
		height = width;
	}

	var canvas = clock_window.find('canvas');
	canvas.attr('width', width);
	canvas.attr('height', height);

	clock_show_time(clock_window);
}

function clock_draw_pointer(ctx, h_width, h_height, angle, size) {
	var x1 = Math.sin(angle);
	var y1 = -Math.cos(angle);
	var x2 = Math.sin(angle + Math.PI / 2);
	var y2 = -Math.cos(angle + Math.PI / 2);
	var x3 = Math.sin(angle + Math.PI);
	var y3 = -Math.cos(angle + Math.PI);
	var x4 = Math.sin(angle + Math.PI * 1.5);
	var y4 = -Math.cos(angle + Math.PI * 1.5);

	ctx.beginPath();
	ctx.moveTo(h_width + x1 * h_width * size, h_height + y1 * h_height * size);
	ctx.lineTo(h_width + x2 * h_width * CLOCK_WIDTH,  h_height + y2 * h_height * CLOCK_WIDTH);
	ctx.lineTo(h_width + x3 * h_width * CLOCK_WIDTH * CLOCK_TAIL,  h_height + y3 * h_height * CLOCK_WIDTH * CLOCK_TAIL);
	ctx.lineTo(h_width + x4 * h_width * CLOCK_WIDTH,  h_height + y4 * h_height * CLOCK_WIDTH);
	ctx.lineTo(h_width + x1 * h_width * size, h_height + y1 * h_height * size);
	ctx.fill();
}

function clock_show_time(clock_window) {
	var time = clock_window.data('time');

	var digital = clock_window.find('div.digital');
	var hours = time[0].toString().padStart(2, '0');
	var minutes = time[1].toString().padStart(2, '0');
	var seconds = time[2].toString().padStart(2, '0');
	digital.text(hours + ':' + minutes + ':' + seconds);

	if (clock_window.find('canvas:visible').length == 0) {
		return;
	}

	var canvas = clock_window.data('canvas');
	var ctx = clock_window.data('ctx');

	var width = Math.round(clock_window.find('canvas').width());
	var height = Math.round(clock_window.find('canvas').height());
	var h_height = Math.round(height / 2)
	var h_width = Math.round(width / 2)

	var seconds = time[2];
	var minutes = time[1] + (seconds / 60);
	var hours = time[0] + (minutes / 60);

	var tpi = Math.PI * 2;
	var step_big = tpi / 12;
	var step_small = tpi / 60;

	/* Init
	 */
	ctx.clearRect(0, 0, canvas.width(), canvas.height());

	/* Markings
	 */
	ctx.lineWidth = 3;
	ctx.strokeStyle = '#808080';

	for (var r = 0; r < tpi; r += step_big) {
		ctx.beginPath();
		var x = Math.sin(r);
		var y = -Math.cos(r);
		ctx.moveTo(h_width + x * h_width * 0.9, h_height + y * h_height * 0.9 );
		ctx.lineTo(h_width + x * h_width, h_height + y * h_height);
		ctx.stroke();
	}

	/* Minutes
	 */
	ctx.fillStyle = '#000000';
	clock_draw_pointer(ctx, h_width, h_height, step_small * minutes, 0.85);

	/* Hour
	 */
	ctx.lineWidth = 1;
	ctx.fillStyle = '#606060';
	clock_draw_pointer(ctx, h_width, h_height, step_big * hours, 0.6);

	/* Seconds
	 */
	ctx.lineWidth = 2;
	ctx.strokeStyle = '#a00000';

	var x = Math.sin(step_small * seconds);
	var y = -Math.cos(step_small * seconds);

	ctx.beginPath();
	ctx.moveTo(h_width, h_height);
	ctx.lineTo(h_width + x * h_width * 0.85, h_height + y * h_height * 0.85);
	ctx.stroke();
}

function clock_menu_click(clock_window, item) {
	switch (item) {
		case 'Time':
			var d = new Date();
			var time = [ d.getHours(), d.getMinutes(), d.getSeconds() ];
			clock_window.data('time', time);

			clock_window.find('div.paused').hide();
			clock_window.data('mode', CLOCK_MODE_TIME);
			clock_window.css('cursor', 'default');
			clock_show_time(clock_window);
			clock_window.set_header();
			break;
		case 'Stopwatch':
			var time = [ 0, 0, 0 ];
			clock_window.data('time', time);

			clock_window.find('div.paused').hide();
			clock_window.data('mode', CLOCK_MODE_STOPWATCH);
			clock_window.data('status', CLOCK_STATUS_RESET);
			clock_window.css('cursor', 'pointer');
			clock_show_time(clock_window);
			clock_window.set_header('Stopwatch');
			break;
		case 'Countdown':
			clock_window.find('div.paused').hide();
			clock_mode_countdown(clock_window);
			clock_window.css('cursor', 'pointer');
			break;
		case 'Analog':
			clock_window.find('canvas').show();
			clock_window.find('div.digital').hide();
			clock_show_time(clock_window);
			break;
		case 'Digital':
			clock_window.find('canvas').hide();
			clock_window.find('div.digital').show();
			clock_show_time(clock_window);
			break;
		case 'Exit':
			clock_window.close();
			break;
		case 'About':
			var message = [
				'<img src="' + CLOCK_ICON + '" class="about" draggable="false" />Clock',
				'Copyright (c) by Hugo Leisink', '',
				'Click on the clock to start or pause the stopwatch or the countdown.'
			];
			orb_alert(message.join('\n'), 'About');
			break;
	}
}

var counter = 0;

function clock_open(filename = undefined) {
	var window_content =
		'<div class="clock">' +
		'<div class="paused">Paused</div>' +
		'<div class="digital"></div>' +
		'<canvas class="analog" />' +
		'</div>';

	var clock_window = $(window_content).orb_window({
		header:'Clock',
		icon: CLOCK_ICON,
		minWidth: 200,
		width: 250,
		height: 250,
		menu: {
			'Clock': [ 'Time', 'Stopwatch', 'Countdown', '-', 'Exit' ],
			'View': [ 'Analog', 'Digital' ],
			'Help': [ 'About' ]
		},
		menuCallback: clock_menu_click,
		close: function() {
			clearInterval(interval);
		},
		resize: function() {
			clock_resize(clock_window);
		}
	});

	clock_window.open();

	var canvas = clock_window.find('canvas');
	var ctx = canvas[0].getContext('2d');
	clock_window.data('canvas', canvas);
	clock_window.data('ctx', ctx);
	clock_window.data('mode', CLOCK_MODE_TIME);
	clock_window.data('status', CLOCK_STATUS_RESET);

	var d = new Date();
    var time = [ d.getHours(), d.getMinutes(), d.getSeconds() ];
	clock_window.data('time', time);

	clock_resize(clock_window);

	clock_window.parent().parent().on('resize', function() {
		clock_resize(clock_window);
	});

	var interval = setInterval(function() {
		var time = clock_window.data('time');
		var status = clock_window.data('status');

		switch (clock_window.data('mode')) {
			case CLOCK_MODE_TIME:
				if (++time[2] == 60) {
					time[2] = 0;
					if (++time[1] == 60) {
						time[1] = 0;
						if (++time[0] == 24) {
							time[0] = 0;
						}
					}
				}
				break;
			case CLOCK_MODE_STOPWATCH:
				if (status != CLOCK_STATUS_RUNNING) {
					break;
				}

				if (++time[2] == 60) {
					time[2] = 0;
					if (++time[1] == 60) {
						time[1] = 0;
						if (++time[0] == 100) {
							time[0] = 0;
						}
					}
				}
				break;
			case CLOCK_MODE_COUNTDOWN:
				if (status != CLOCK_STATUS_RUNNING) {
					break;
				}

				time[2] -= 1;

				if ((time[2] == 0) && (time[1] == 0) && (time[2] == 0)) {
					clock_window.data('status', CLOCK_STATUS_STOPPED);
					orb_alert('Countdown done.', 'Clock');
				} else if (time[2] == -1) {
					time[2] = 59;
					if (--time[1] == -1) {
						time[1] = 59;
						time[0] -=1;
					}
				}
				break;
		}

		clock_window.data('time', time);

		clock_show_time(clock_window);
	}, 1000);

	clock_window.on('click', function() {
		var mode = clock_window.data('mode');
		if (mode == CLOCK_MODE_TIME) {
			return;
		}

		var status = clock_window.data('status');

		switch (status) {
			case CLOCK_STATUS_RESET:
			case CLOCK_STATUS_PAUSED:
				status = CLOCK_STATUS_RUNNING;
				clock_window.find('div.paused').hide();
				break;
			case CLOCK_STATUS_RUNNING:
				status = CLOCK_STATUS_PAUSED;
				clock_window.find('div.paused').show();
				break;
		}

		clock_window.data('status', status);
	});
}

$(document).ready(function() {
	orb_startmenu_add('Clock', CLOCK_ICON, clock_open);
});
