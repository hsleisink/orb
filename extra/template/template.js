/* Orb template application
 *
 * Copyright (c) by NAME
 *
 * Let every function in this file start with the application name
 * to avoid conflicts with other applications.
 *
 * Always use template_window to interact with your application to
 * avoid issues with multiple instances of your application.
 */

const TEMPLATE_ICON = '/images/application.png';

function template_menu_click(template_window, item) {
	switch (item) {
		case 'Exit':
			template_window.close();
			break;
		case 'About':
			orb_alert('<img src="' + TEMPLATE_ICON + '" class="about" draggable="false" />Template\nCopyright (c) by NAME', 'About');
			break;
	}
}

function template_open(filename = undefined) {
	var window_content =
		'<div class="template">' +
		'<p>This is the Orb Template application.</p>' +
		'</div>';

	var template_window = $(window_content).orb_window({
		header:'Template',
		icon: TEMPLATE_ICON,
		width: 400,
		height: 200,
		menu: {
			'File': [ 'Exit' ],
			'Help': [ 'About' ]
		},
		menuCallback: template_menu_click
	});

	template_window.open();

	if (filename != undefined) {
	}
}

$(document).ready(function() {
	orb_startmenu_add('Template', TEMPLATE_ICON, template_open);
});
